package socialnetwork.service;

import socialnetwork.domain.User;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.repository.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * UserService class.
 * Class used to incorporate all the important aspects of
 * managing a list of users.
 */
public class UserService {
    private Repository<Long, User> repo;

    public UserService(Repository<Long, User> repo) {
        this.repo = repo;
    }

    /**
     * Method to decode an input line and obtain an user.
     *
     * @param line - string to be decoded
     * @return a 'brute' user.
     */
    private User extractUser(String line) {
        List<String> attributes = Arrays.asList(line.split(";"));
        User user = null;
        if(attributes.size()==4)
            user = new User(attributes.get(1), attributes.get(2),attributes.get(3));
        if(attributes.size()==5)
            user = new User(attributes.get(1),attributes.get(2),attributes.get(3),attributes.get(4));
        if(user != null)
            user.setId(Long.valueOf(attributes.get(0)));
        return user;
    }

    /**
     * Method to add an user in the list of users in the program.
     *
     * @param userline - an user encoded into a string.
     * @return user value from method save
     * null if user was added.
     * user to be added otherwise
     * @throws ValidationException      if the entity is not valid
     * @throws IllegalArgumentException if the given entity is null.
     */
    public User addUser(String userline) {
        User user = repo.save(extractUser(userline));
        return user;
    }

    /**
     * Method to obtain the list of users.
     *
     * @return the list of users.
     */
    public Iterable<User> getAll() {
        return repo.findAll();
    }

    /**
     * Method to remove an user from the list of users in the program.
     *
     * @param id - an user unique identifier
     * @return user value from the method delete
     * null if there's no user matching the given one
     * the given user otherwise.
     * @throws IllegalArgumentException if the given user contains a null identifier.
     */
    public User deleteUser(Long id) {
        User user = repo.delete(id);
        return user;
    }

    /**
     * Method to update an user from the list of users in the program.
     *
     * @param userline - an user encoded into a string
     * @return - null, if the given user was updated;
     *           user decoded from userline, otherwise.
     */
    public User updateUser(String userline) {
        User updated = extractUser(userline);
        User old = findUser(updated.getId());
        User user = repo.update(updated);
        if (old != null && user == null) {
            updated = findUser(updated.getId());
            old.getFriends().forEach(updated::saveFriend);
        }
        return user;
    }

    /**
     * Method to find an user in the list of users in the program.
     *
     * @param id - unique identifier for an user
     * @return the user matching the given id
     * null, if there are no users matching given identifier
     * @throws IllegalArgumentException if the given identifier is null
     */
    public User findUser(Long id) {
        User user = repo.findOne(id);
        return user;
    }

    public Long getAvailableID() {
        List<Long> userIDs = new ArrayList<>();
        this.repo.findAll().forEach(u -> userIDs.add(u.getId()));
        userIDs.sort(new Comparator<Long>() {
            @Override
            public int compare(Long o1, Long o2) {
                if (o1 == o2) return 0;
                if (o1 < o2) return -1;
                return 1;
            }
        });
        if (userIDs.size() == 0)
            return 1l;
        else if (userIDs.get(0) > 1)
            return userIDs.get(0) - 1;
        Long returnedID = userIDs.get(0);
        for (Long id : userIDs)
            if (id == returnedID)
                returnedID++;
            else break;
        return returnedID;
    }

}
