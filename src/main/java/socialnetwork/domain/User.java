package socialnetwork.domain;

import utils.PasswordUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * User-domain class
 */
public class User extends Entity<Long> {
    private String firstName;
    private String lastName;
    private List<Long> friends;
    private String password;
    private String salt;


    public User(String firstName, String lastName, String password, String salt) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.friends = new ArrayList<>();
        this.password = password;
        this.salt = salt;
    }

    public User(String firstName, String lastName, String password){
        this.firstName=firstName;
        this.lastName=lastName;
        this.friends=new ArrayList<>();
        setPassword(password);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.salt = PasswordUtils.getSalt();
        this.password=PasswordUtils.generateSecurePassword(password,this.salt);
    }

    public String getSalt() {
        return salt;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Long> getFriends() {
        return friends;
    }

    public String getName(){
        return this.firstName + " " + this.lastName;
    }

    public String getUserName(){
        String username = firstName.toLowerCase()+"."+lastName.toLowerCase()+"_"+getId();
        return username.replace('-','_');
    }

    public boolean verifyLogin(String username,String password){
        return username.equals(getUserName()) && PasswordUtils.verifyUserPassword(password,this.password,this.salt);
    }

    @Override
    public String toString() {
        return getName();
    }

    /**
     * Method to add a user in this user's friends list.
     *
     * @param id - unique identifier for the user to be added
     * @return this instance of the user, even if the user with the identifier id
     * was added or not.
     */
    public User saveFriend(Long id) {
        if (!friends.contains(id))
            friends.add(id);
        return this;
    }

    /**
     * Method to remove a user from this user's friends list.
     *
     * @param id - unique identifier for the user to be removed.
     * @return this instance of the user, even if the user with the identifier id
     * was in the friends list to be removed or not.
     */
    public User removeFriend(Long id) {
        if (friends.contains(id))
            friends.remove(id);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User that = (User) o;
        return getFirstName().equals(that.getFirstName()) &&
                getLastName().equals(that.getLastName()) &&
                getFriends().equals(that.getFriends());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstName(), getLastName(), getFriends());
    }
}