package socialnetwork.ui.console;

import socialnetwork.domain.User;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.FriendshipService;
import socialnetwork.service.MessageService;
import socialnetwork.service.UserService;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.List;
import java.util.NoSuchElementException;

public class UserConsole extends MainConsole {
    public UserConsole(UserService userService, FriendshipService friendshipService, MessageService messageService) {
        super(userService, friendshipService, messageService);
    }

    @Override
    public void run(){
        while (true) {
            int option;
            System.out.println("1.Add User" + newLine
                    + "2.Remove User" + newLine
                    + "3.Update user" + newLine
                    + "4.View users" + newLine
                    + "5.Return to main menu" + newLine
                    + "Choose an option from main menu by giving a number from the above."
                    + newLine);
            try {
                option = scanner.nextInt();
                switch (option) {
                    case 5: {
                        System.out.println("Closing user menu..." + newLine);
                        return;
                    }
                    case 4: {
                        System.out.println("===========Network users connected============");
                        this.userService.getAll().forEach(System.out::println);
                        System.out.println("=========================");
                        break;
                    }
                    case 3: {
                        System.out.println("Give a user to update." +
                                "User will be given in the next form: oldID;new_firstname;new_lastname"
                                + newLine);
                        String line = scanner.next();
                        User user = this.userService.updateUser(line);
                        if (user == null)
                            System.out.println("User updated successfully!" + newLine);
                        else throw new ConsoleException("No user matching ID=" + user.getId() + "!" + newLine);
                        break;
                    }
                    case 2: {
                        System.out.println("Give a user to remove." +
                                "User will be given in the next form: id;first name;last name"
                                + newLine);
                        String line = scanner.next();
                        List<String> arguments = Arrays.asList(line.split(";"));

                        User user = userService.findUser(Long.parseLong(arguments.get(0)));
                        if (user != null) {
                            System.out.println("Account removed!" + newLine);
                            this.friendshipService.updateLists(user);
                            this.messageService.updateMessages(user);
                            this.userService.deleteUser(0L);//line
                        } else throw new ConsoleException("No account matching this infos:" + line + newLine);
                        break;
                    }
                    case 1: {
                        System.out.println("Give a user to add." +
                                "User will be given in the next form: id;first name;last name"
                                + newLine);
                        String line = scanner.next();
                        User user = this.userService.addUser(line);
                        if (user == null)
                            System.out.println("New account created!" + newLine);
                        else throw new ConsoleException("Already an user with the ID=" + user.getId() + "!" + newLine);
                        break;
                    }
                    default: {
                        System.out.println("No valid option!" + newLine);
                        break;
                    }
                }

            } catch (ValidationException validationException) {
                System.err.println(validationException + newLine);
            } catch (InputMismatchException inputMismatchException) {
                System.err.println("You need to give a number to proceed" + newLine);
                scanner.nextLine();
            } catch (NoSuchElementException noSuchElementException) {
                System.err.println(noSuchElementException + newLine);
                scanner.nextLine();
            } catch (IllegalArgumentException illegalArgumentException) {
                System.err.println("Cannot add to the list!" + newLine);
            } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
                System.err.println("Do not use spaces when you input an user" + newLine);
                scanner.nextLine();
            } catch (ConsoleException consoleException) {
                System.err.println(consoleException + newLine);
            }


        }
    }
}
