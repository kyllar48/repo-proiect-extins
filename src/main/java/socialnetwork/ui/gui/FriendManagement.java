package socialnetwork.ui.gui;

import com.itextpdf.text.Rectangle;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPHeaderCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import socialnetwork.domain.Message;
import socialnetwork.domain.User;
import socialnetwork.service.EventService;
import socialnetwork.service.FriendshipService;
import socialnetwork.service.MessageService;
import socialnetwork.service.UserService;
import socialnetwork.service.wrappers.ExternUser;
import socialnetwork.service.wrappers.MessageView;
import utils.PaginatorUtils;

import java.awt.*;
import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static javafx.scene.text.TextAlignment.CENTER;
import static utils.Constants.*;

public class FriendManagement implements Screen {

    private User logged;
    private ExternUser friend;
    private UserService userService;
    private FriendshipService friendshipService;
    private MessageService messageService;
    private EventService eventService;
    private List<Long> blockedUsers;

    private ScreensController myController;

    ObservableList<User> friendsViewerList = FXCollections.observableArrayList();
    ObservableList<MessageView> msgList = FXCollections.observableArrayList();

    @FXML
    Label friendFriendsLabel;
    @FXML
    ListView<User> friendsOfFriendListView;
    @FXML
    Pagination friendsOfFriendPaginator;

    private void getViewedFriends() {
        friendFriendsLabel.setText(friend.getName() + "'s friends!");
        friendsViewerList.setAll(getFriendsOfFriendList());
        PaginatorUtils.getListPager(friendsOfFriendPaginator, friendsOfFriendListView, friendsViewerList);
    }

    private List<User> getFriendsOfFriendList() {
        return this.friend.getUser().getFriends()
                .stream().map(id -> this.userService.findUser(id)).collect(Collectors.toList());
    }

    @FXML
    TableView<MessageView> messageTable;
    @FXML
    TableColumn<MessageView, String> textTableColumn;
    @FXML
    TableColumn<MessageView, String> dateTableColumn;
    @FXML
    Label messageNOLabel;

    @Override
    public void setScreenParent(ScreensController screenPage) {
        myController = screenPage;
        String title = friend.getName() + "'s informations page";
        Stage myStage = (Stage) myController.getScene().getWindow();
        myStage.setTitle(title);
    }

    @Override
    public void setEnvironment(List<Object> args) {
        this.userService = (UserService) args.get(0);
        this.friendshipService = (FriendshipService) args.get(1);
        this.messageService = (MessageService) args.get(2);
        this.eventService = (EventService) args.get(3);
        this.logged = (User) args.get(4);
        this.friend = (ExternUser) args.get(5);
        blockedUsers = this.friendshipService.getBlockedUsers(logged, f -> f.getId().getLeft() == logged.getId())
                .stream().map(elem -> elem.getLeft().getRight()).collect(Collectors.toList());
        initializeInterface();
    }

    private List<MessageView> getMessageList() {
        List<Message> messages = this.messageService.showConversation(this.logged, this.friend.getUser());
        if (messages.size() == 1)
            messageNOLabel.setText("1 message available!");
        else messageNOLabel.setText(messages.size() + " messages available!");
        return messages.stream().map(MessageView::new).collect(Collectors.toList());
    }

    @FXML
    ImageView friendPic;

    private void initializeInterface() {
        getViewedFriends();
        textTableColumn.setCellValueFactory((new PropertyValueFactory<>("text")));
        dateTableColumn.setCellValueFactory((new PropertyValueFactory<>("date")));

        FileInputStream input = null;
        try {
            input = new FileInputStream(PICS_PATH + friend.getUser().getUserName() + ".jpg");
        } catch (Exception e) {
            try {
                input = new FileInputStream(PICS_PATH + "unknown.jpg");
            } catch (FileNotFoundException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }
        }
        friendPic.setImage(new Image(input));

        updateMessageTable();
    }

    @FXML
    Pagination messagePaginator;

    public void updateMessageTable() {
        msgList.setAll(getMessageList());
        PaginatorUtils.getTablePager(messagePaginator, messageTable, msgList);
    }

    public void closeThisMenu(ActionEvent actionEvent) {
        myController.unloadScreen(screen2ID);
        List<Object> objectList = Arrays.asList(this.userService, this.friendshipService, this.messageService, this.eventService, this.logged);
        myController.loadScreen(screen2ID, screen2Res, objectList);
        myController.setScreen(screen2ID);

        myController.unloadScreen(screen3ID);
    }

    public void removeFriendHandler(ActionEvent actionEvent) {
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setOpacity(1);
        stage.setResizable(false);
        stage.setTitle("Warning");

        Label infoLabel = new Label("Are you sure you want to remove this friend?");
        infoLabel.setTextAlignment(CENTER);
        infoLabel.setWrapText(true);

        Button yesButton = new Button("Yes");
        Button noButton = new Button("No");
        yesButton.setPrefSize(50, 20);
        noButton.setPrefSize(50, 20);

        HBox optionBox = new HBox(10, yesButton, noButton);
        VBox mainBox = new VBox(10, infoLabel, optionBox);

        yesButton.setOnAction(e -> {
            friendshipService.removeFriend(logged, friend.getUser());
            friendshipService.removeFriend(friend.getUser(), logged);
            myController.unloadScreen(screen2ID);
            List<Object> objectList = Arrays.asList(userService, friendshipService, messageService, eventService, logged);
            myController.loadScreen(screen2ID, screen2Res, objectList);

            MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Success", friend.getName() + " was successfully removed from your friends!");

            myController.setScreen(screen2ID);
            myController.unloadScreen(screen3ID);
            stage.close();
        });
        noButton.setOnAction(e -> stage.close());

        optionBox.setPadding(new Insets(20, 10, 20, 10));

        mainBox.setAlignment(Pos.CENTER);
        optionBox.setAlignment(Pos.CENTER);

        Scene sc = new Scene(mainBox, 250, 150);
        stage.setScene(sc);
        sc.getStylesheets().add(styling1Res);
        stage.showAndWait();
    }

    public void handleMessageItemClicked(MouseEvent mouseEvent) {
        try {
            MessageView messageView = messageTable.getSelectionModel().getSelectedItem();
            if (messageView.getMessage().getId() >= 1000)//is a reply to all
                replyToAllPopUp(messageView);
            else if (messageView.getMessage().getFrom() == logged.getId())
                yourMessagePopUp(messageView);
            else friendMessagePopUp(messageView);
            messageTable.getSelectionModel().clearSelection();
        } catch (NullPointerException n) {
        }
    }

    private void yourMessagePopUp(MessageView msg) {
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.setTitle("Your message View-and-Actions Pop-up");

        boolean hasReply = msg.getMessage().getReply() != null;
        String replyText = hasReply ? msg.getMessage().getReply().getMessage() : "N\\A";
        String replyFrom = hasReply ? this.userService.findUser(msg.getMessage().getReply().getFrom()).getName() : "N\\A";

        Label msgDateTXT = new Label("Date");
        Label msgDateValue = new Label(msg.getDate());
        Label msgToTXT = new Label("To");
        ComboBox<User> msgToValue = new ComboBox<>(
                FXCollections.observableArrayList(msg.getMessage().getTo().stream().map(userService::findUser).collect(Collectors.toList())));
        msgToValue.setPromptText("receivers");
        Label msgMessageTXT = new Label("Message");
        Label msgMessageValue = new Label(msg.getText());
        Label msgReplyTXT = new Label("Reply");
        Label msgReplyValue = new Label(replyText);
        Label msgReplyFromTXT = new Label("Sent by");
        Label msgReplyFromValue = new Label(replyFrom);

        GridPane gridPane = new GridPane();

        gridPane.addRow(0, msgDateTXT, msgDateValue);
        gridPane.addRow(1, msgToTXT, msgToValue);
        gridPane.addRow(2, msgMessageTXT, msgMessageValue);
        gridPane.addRow(3, msgReplyTXT, msgReplyValue);
        gridPane.addRow(4, msgReplyFromTXT, msgReplyFromValue);
        gridPane.setPadding(new Insets(20, 10, 20, 10));

        Button cancelButton = new Button("Cancel");
        cancelButton.setPrefSize(165, 20);
        Button removeMessageButton = new Button("Remove Message");
        removeMessageButton.setPrefSize(165, 20);
        Button removeReplyButton = new Button("Remove Reply");
        removeReplyButton.setPrefSize(165, 20);

        Pane buttonBox = null;
        if (hasReply) {
            VBox vBox = new VBox(10);
            vBox.setAlignment(Pos.CENTER);
            HBox actionButtonBox = new HBox(10);
            actionButtonBox.setAlignment(Pos.CENTER);
            actionButtonBox.getChildren().addAll(removeMessageButton, removeReplyButton);

            buttonBox = vBox;
            buttonBox.getChildren().addAll(actionButtonBox, cancelButton);
        } else {
            HBox hBox = new HBox(10);
            hBox.setAlignment(Pos.CENTER);

            buttonBox = hBox;
            buttonBox.getChildren().addAll(removeMessageButton, cancelButton);
        }

        cancelButton.setOnAction(e -> stage.close());
        removeMessageButton.setOnAction(event -> {
            if (hasReply)
                messageService.clearMessage(msg.getMessage().getId() * 1000);
            messageService.clearMessage(msg.getMessage().getId());
            MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Success", "Your message informations were deleted!");
            updateMessageTable();

            stage.close();
        });
        removeReplyButton.setOnAction(event -> {
            messageService.clearMessage(msg.getMessage().getId() * 1000);
            MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Success", "Reply informations were deleted!");
            updateMessageTable();
            stage.close();
        });

        VBox centralBox = new VBox(5, gridPane, buttonBox);

        gridPane.setHgap(10.0);
        gridPane.setVgap(10.0);
        gridPane.setAlignment(Pos.CENTER);

        buttonBox.setPadding(new Insets(10, 20, 10, 20));
        centralBox.setAlignment(Pos.CENTER);

        Scene scene = new Scene(centralBox, 500, 300);
        stage.setScene(scene);
        scene.getStylesheets().add(styling1Res);
        stage.showAndWait();

    }

    private void friendMessagePopUp(MessageView msg) {
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.setTitle(friend.getName() + "'s message View-and-Actions Pop-up");

        boolean hasReply = msg.getMessage().getReply() != null;
        String replyText = hasReply ? msg.getMessage().getReply().getMessage() : "N\\A";
        String replyFrom = hasReply ? this.userService.findUser(msg.getMessage().getReply().getFrom()).getName() : "N\\A";

        Label msgDateTXT = new Label("Date");
        Label msgDateValue = new Label(msg.getDate());
        Label msgToTXT = new Label("To");
        ComboBox<User> msgToValue = new ComboBox<>(
                FXCollections.observableArrayList(msg.getMessage().getTo().stream().map(userService::findUser).collect(Collectors.toList())));
        msgToValue.setPromptText("receivers");
        Label msgMessageTXT = new Label("Message");
        Label msgMessageValue = new Label(msg.getText());
        Label msgReplyTXT = new Label("Reply");
        Label msgReplyValue = new Label(replyText);
        Label msgReplyFromTXT = new Label("Sent by");
        Label msgReplyFromValue = new Label(replyFrom);

        Button normalReplyButton = hasReply ? new Button("Edit As Reply") : new Button("Send As Reply");
        normalReplyButton.setPrefSize(200, 20);
        Button replyToAllButton = hasReply ? new Button("Edit As Reply-to-All") : new Button("Send As Reply-to-All");
        replyToAllButton.setPrefSize(200, 20);
        Button removeReplyButton = new Button("Remove My Reply");
        removeReplyButton.setPrefSize(200, 20);
        Button cancelButton = new Button("Cancel");
        cancelButton.setPrefSize(200, 20);

        TextField responseText = new TextField();

        cancelButton.setOnAction(e -> stage.close());
        removeReplyButton.setOnAction(event -> {
            messageService.clearMessage(msg.getMessage().getId() * 1000);
            MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Success", "Reply informations were deleted!");
            updateMessageTable();
            stage.close();
        });
        normalReplyButton.setOnAction(event -> {
            if (responseText.getText().strip().length() == 0)
                MessageAlert.showErrorMessage(null, "You need to type a message in the destinated field before clicking this button!");
            else {
                String messageLine = msg.getMessage().getId() + ";" + msg.getMessage().getFrom() + ";" + responseText.getText().strip();
                if (hasReply)
                    messageService.clearMessage(msg.getMessage().getId() * 1000);
                messageService.sendMessage(logged, messageLine, true, blockedUsers);
                MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Success", "Reply sent!");
                updateMessageTable();
                stage.close();
            }
        });
        replyToAllButton.setOnAction(event -> {
            if (responseText.getText().strip().length() == 0)
                MessageAlert.showErrorMessage(null, "You need to type a message in the destinated field before clicking this button!");
            else {
                List<Long> to = new ArrayList<>(msg.getMessage().getTo());
                to.add(friend.getUser().getId());
                to.remove(logged.getId());
                String messageLine = msg.getMessage().getId() + ";" +
                        to.stream().map(x -> x.toString()).reduce("", (x, y) -> x.concat(",").concat(y)).substring(1) + ";" + responseText.getText().strip();
                if (hasReply)
                    messageService.clearMessage(msg.getMessage().getId() * 1000);
                messageService.sendMessage(logged, messageLine, true, blockedUsers);
                MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Success", "Reply-to-All sent!");
                updateMessageTable();
                stage.close();
            }
        });

        HBox buttonBox1 = new HBox(15);
        HBox buttonBox2 = new HBox(15);
        GridPane infoPane = new GridPane();

        buttonBox1.getChildren().addAll(normalReplyButton, replyToAllButton);

        if (hasReply)
            if (msg.getMessage().getReply().getFrom() == logged.getId())
                buttonBox2.getChildren().add(removeReplyButton);
        buttonBox2.getChildren().add(cancelButton);

        infoPane.addRow(0, msgDateTXT, msgDateValue);
        infoPane.addRow(1, msgToTXT, msgToValue);
        infoPane.addRow(2, msgMessageTXT, msgMessageValue);
        infoPane.addRow(3, msgReplyTXT, msgReplyValue);
        infoPane.addRow(4, msgReplyFromTXT, msgReplyFromValue);

        infoPane.setPadding(new Insets(20, 10, 20, 10));

        VBox mainPane = new VBox(10);

        mainPane.getChildren().addAll(infoPane, responseText, buttonBox1, buttonBox2);

        infoPane.setHgap(15.0);
        infoPane.setVgap(15.0);
        infoPane.setAlignment(Pos.CENTER);

        buttonBox1.setAlignment(Pos.CENTER);

        Insets insets = new Insets(10, 20, 10, 20);

        buttonBox1.setPadding(insets);

        buttonBox2.setAlignment(Pos.CENTER);
        buttonBox2.setPadding(insets);

        mainPane.setAlignment(Pos.CENTER);
        mainPane.setPadding(insets);
        responseText.setPromptText("you can write replies here...");

        Scene sc = new Scene(mainPane, 550, 400);
        stage.setScene(sc);
        sc.getStylesheets().add(styling1Res);
        stage.showAndWait();
    }

    private void replyToAllPopUp(MessageView msg) {
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setOpacity(1);
        stage.setResizable(false);
        stage.setTitle("Message View-and-Actions Pop-up");

        User from = this.userService.findUser(msg.getMessage().getFrom());

        User fromRoot = this.userService.findUser(this.messageService.findMessage(msg.getMessage().getId() / 1000).getFrom());
        String textRoot = this.messageService.findMessage(msg.getMessage().getId() / 1000).getMessage();

        //labels
        Label msgDateTXT = new Label("Date");
        Label msgDateValue = new Label(msg.getDate());
        Label msgFromTXT = new Label("From");
        Label msgFromValue = new Label(from.getName());
        Label msgToTXT = new Label("To");
        ComboBox<User> msgToValue = new ComboBox<>(
                FXCollections.observableArrayList(msg.getMessage().getTo().stream().map(userService::findUser).collect(Collectors.toList())));
        msgToValue.setPromptText("receivers");
        Label msgMessageTXT = new Label("Message");
        Label msgMessageValue = new Label(msg.getText());
        Label msgInfo1TXT = new Label("Replied to");
        Label msgInfo1Value = new Label(textRoot);
        Label msgInfo2TXT = new Label("Sent by");
        Label msgInfo2Value = new Label(fromRoot.getName());

        GridPane gridPane = new GridPane();

        Button cancelButton = new Button("Cancel");
        cancelButton.setPrefSize(150, 20);
        Button changeReplyAllText = new Button("Modify message");
        changeReplyAllText.setPrefSize(150, 20);

        TextField replyText = new TextField();
        replyText.setPromptText("you can type a new reply for this message ...");

        HBox controlBox = new HBox(20, changeReplyAllText, cancelButton);
        VBox centralBox = new VBox(10, gridPane, replyText, controlBox);

        changeReplyAllText.setOnAction(event -> {
            if (replyText.getText().strip().length() == 0)
                MessageAlert.showErrorMessage(null, "You need to type a message in the destinated field before clicking this button!");
            else {
                String responseText = replyText.getText().strip();
                String messageLine = msg.getMessage().getId() / 1000 + ";";
                List<Long> toListReply = new ArrayList<>(msg.getMessage().getTo());
                toListReply.add(msg.getMessage().getFrom());
                toListReply.remove(logged.getId());
                messageLine += toListReply.stream().map(Objects::toString)
                        .reduce("", (x, y) -> x.concat(",").concat(y)).substring(1) + ";" + responseText;
                messageService.clearMessage(msg.getMessage().getId());
                messageService.sendMessage(logged, messageLine, true, blockedUsers);
                MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Success", "Reply changed and sent!");
                updateMessageTable();
                stage.close();
            }
        });
        cancelButton.setOnAction(e -> stage.close());

        gridPane.addRow(0, msgDateTXT, msgDateValue);
        gridPane.addRow(1, msgFromTXT, msgFromValue);
        gridPane.addRow(2, msgToTXT, msgToValue);
        gridPane.addRow(3, msgMessageTXT, msgMessageValue);
        gridPane.addRow(4, msgInfo1TXT, msgInfo1Value);
        gridPane.addRow(5, msgInfo2TXT, msgInfo2Value);

        gridPane.setAlignment(Pos.CENTER);
        gridPane.setHgap(10.0);
        gridPane.setVgap(10.0);
        gridPane.setPadding(new Insets(10, 20, 10, 20));

        controlBox.setAlignment(Pos.CENTER);
        controlBox.setPadding(new Insets(10, 20, 10, 20));
        centralBox.setAlignment(Pos.CENTER);
        centralBox.setPadding(new Insets(0, 40, 0, 40));

        Scene sc = new Scene(centralBox, 550, 350);
        stage.setScene(sc);
        sc.getStylesheets().add(styling1Res);
        stage.showAndWait();

    }

    @FXML
    Button messagesByPeriodButton;
    @FXML
    DatePicker sinceDatePicker;
    @FXML
    DatePicker untilDatePicker;


    public void seeMessagesHandler(ActionEvent actionEvent) {
        LocalDate date1 = sinceDatePicker.getValue();
        LocalDate date2 = untilDatePicker.getValue();
        if (date2 == null)
            date2 = LocalDate.MAX;
        if (date1 == null)
            date1 = LocalDate.MIN;
        if (date1.isAfter(date2))
            MessageAlert.showErrorMessage(null, "Selected dates do not represent a valid period of time!");
        else if (msgList.size() == 0)
            MessageAlert.showErrorMessage(null, "No messages to see!");
        else {
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setResizable(false);
            stage.setTitle("Messages Preview");

            final LocalDate sinceDate = date1;
            final LocalDate untilDate = date2;

            Pagination filteredMessagePaginator = new Pagination();

            TableView<MessageView> filteredMessagesTable = new TableView<>();
            TableColumn<MessageView, String> messageTextColumn = new TableColumn<>("Message Text");
            TableColumn<MessageView, String> messageDateTimeColumn = new TableColumn<>("Sent at:");

            filteredMessagesTable.getColumns().setAll(messageDateTimeColumn, messageTextColumn);

            ObservableList<MessageView> observableMessageList = FXCollections.observableArrayList();
            observableMessageList.setAll(this.messageService.showConversation(this.logged, this.friend.getUser())
                    .stream().map(MessageView::new).collect(Collectors.toList()));
            FilteredList<MessageView> filteredMessageList = new FilteredList<>(observableMessageList, data -> true);
            filteredMessageList.setPredicate(m -> {
                LocalDate msgDate = LocalDate.from(m.getMessage().getData());
                return (msgDate.isAfter(sinceDate) || msgDate.isEqual(sinceDate)) && (msgDate.isBefore(untilDate) || msgDate.isEqual(untilDate));
            });

            messageTextColumn.setCellValueFactory((new PropertyValueFactory<>("text")));
            messageDateTimeColumn.setCellValueFactory((new PropertyValueFactory<>("date")));

            messageTextColumn.setMinWidth(225);
            messageTextColumn.setMaxWidth(225);
            messageDateTimeColumn.setMinWidth(150);
            messageDateTimeColumn.setMaxWidth(150);

            filteredMessagePaginator.setMaxSize(375, 300);
            filteredMessagePaginator.setMinSize(375, 300);

            PaginatorUtils.getTablePager(filteredMessagePaginator, filteredMessagesTable, filteredMessageList);

            TextField filenameField = new TextField();
            filenameField.setPrefSize(315, 25);
            filenameField.setMaxSize(315, 25);
            filenameField.setMinSize(315, 25);
            filenameField.setPromptText("insert your pdf filename here ...");

            Button savePDFButton = new Button("Save as PDF");
            Button exitButton = new Button("Exit");

            savePDFButton.setPrefSize(150, 20);
            exitButton.setPrefSize(150, 20);

            savePDFButton.setFont(new javafx.scene.text.Font("Comic Sans MS Bold", 16.0));
            exitButton.setFont(new javafx.scene.text.Font("Comic Sans MS Bold", 16.0));

            Integer allSentNO = filteredMessageList.stream().map(m -> {
                if (m.getMessage().getReply() != null)
                    return 2;
                return 1;
            }).reduce(0, (x, y) -> x + y);
            Integer allSentByMe = filteredMessageList.stream().map(MessageView::getMessage).map(m -> {
                if (m.getReply() != null)
                    if (m.getReply().getFrom() == logged.getId())
                        return 1;
                if (m.getFrom() == logged.getId())
                    return 1;
                return 0;
            }).reduce(0, (x, y) -> x + y);
            Integer allSentByFriend = filteredMessageList.stream().map(MessageView::getMessage).map(m -> {
                if (m.getReply() != null)
                    if (m.getReply().getFrom() == friend.getUser().getId())
                        return 1;
                if (m.getFrom() == friend.getUser().getId())
                    return 1;
                return 0;
            }).reduce(0, (x, y) -> x + y);
            Integer externMessages = allSentNO - allSentByMe - allSentByFriend;

            ObservableList<PieChart.Data> pieChartData =
                    FXCollections.observableArrayList();
            if (allSentByMe != 0)
                pieChartData.add(new PieChart.Data("Messages sent by you (" + allSentByMe + ")", allSentByMe * 100 / allSentNO));
            if (allSentByFriend != 0)
                pieChartData.add(new PieChart.Data("Messages sent by " + friend.getUser().getLastName() + " (" + allSentByFriend + ")", allSentByFriend * 100 / allSentNO));
            if (externMessages != 0)
                pieChartData.add(new PieChart.Data("Messages from extern sources (" + externMessages + ")", externMessages * 100 / allSentNO));

            PieChart pieChart = new PieChart();
            pieChart.setTitle("Messages + Replies Chart\nTotal messages: " + allSentNO);
            pieChart.setData(pieChartData);
            pieChart.setPrefSize(350, 300);
            pieChart.setMinSize(350, 300);
            pieChart.setMaxSize(350, 300);

            HBox infoBox = new HBox(10, filteredMessagePaginator);
            HBox buttonBox = new HBox(15, savePDFButton, exitButton);
            if (allSentNO != 0)
                infoBox.getChildren().add(pieChart);

            VBox mainBox = new VBox(15, infoBox, filenameField, buttonBox);

            savePDFButton.setOnAction(e -> {
                try {
                    String filename = filenameField.getText().strip();
                    if (filename.length() == 0)
                        MessageAlert.showErrorMessage(null, "You did not specify the filename for the PDF!");
                    else if (!filename.endsWith(".pdf") || filename.length() == 4 || filename.contains("/") || filename.contains(("\\")))
                        MessageAlert.showErrorMessage(null, "Use file_name.pdf format to save the file!");
                    else {
                        Document document = new Document();
                        PdfWriter.getInstance(document, new FileOutputStream(REPORTS_PATH + filename));

                        document.open();

                        com.itextpdf.text.Font font = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 10, BaseColor.RED);
                        Paragraph title = new Paragraph("Messages from the selected period\n" + setPeriod(sinceDate, untilDate), FontFactory.getFont(FontFactory.HELVETICA_BOLD, 15, BaseColor.BLACK));
                        title.setAlignment(Rectangle.ALIGN_CENTER);
                        document.add(title);
                        document.add(new Phrase("\n\n"));

                        PdfPTable messagePdfTable = new PdfPTable(6);
                        Stream.of(new Chunk("Date", font), new Chunk("From", font), new Chunk("To", font),
                                new Chunk("Message text", font), new Chunk("Reply Sender", font), new Chunk("Reply Message", font))
                                .forEach(columnTitle -> {
                                    PdfPHeaderCell header = new PdfPHeaderCell();
                                    header.setBackgroundColor(BaseColor.YELLOW);
                                    header.setBorderWidth(1);

                                    header.setHorizontalAlignment(Element.ALIGN_CENTER);
                                    header.setVerticalAlignment(Element.ALIGN_CENTER);

                                    Phrase phrase = new Phrase(columnTitle);
                                    Paragraph paragraph = new Paragraph(phrase);
                                    paragraph.setAlignment(Rectangle.ALIGN_CENTER);
                                    header.setPhrase(phrase);

                                    messagePdfTable.addCell(header);
                                });

                        filteredMessageList.forEach(m -> addMessageToTable(messagePdfTable, m.getMessage()));

                        document.add(messagePdfTable);
                        document.close();

                        MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Success", filename + " was generated!");
                        stage.close();

                        if (Desktop.isDesktopSupported()) {
                            Desktop desktop = Desktop.getDesktop();
                            File file = new File(REPORTS_PATH + filename);
                            desktop.open(file);
                        }
                    }
                } catch (FileNotFoundException | DocumentException f) {
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            });
            exitButton.setOnAction(e -> stage.close());

            Insets insets = new Insets(10, 20, 10, 20);
            buttonBox.setPadding(insets);
            mainBox.setPadding(insets);
            infoBox.setAlignment(Pos.CENTER);
            mainBox.setAlignment(Pos.CENTER);
            buttonBox.setAlignment(Pos.CENTER);

            Scene sc = new Scene(mainBox, 800, 500);
            stage.setScene(sc);
            sc.getStylesheets().add(styling1Res);
            stage.showAndWait();
        }
        sinceDatePicker.setValue(null);
        untilDatePicker.setValue(null);
    }

    private String setPeriod(LocalDate sinceDate, LocalDate untilDate) {
        if (sinceDate == LocalDate.MIN && untilDate == LocalDate.MAX)
            return "(entire conversation)";
        else if (sinceDate == LocalDate.MIN)
            return "(until " + untilDate.format(DATE_FORMATTER) + ")";
        else if (untilDate == LocalDate.MAX)
            return "(since " + sinceDate.format(DATE_FORMATTER) + ")";
        return "(" + sinceDate.format(DATE_FORMATTER) + "  -  " + untilDate.format(DATE_FORMATTER) + ")";
    }

    private void addMessageToTable(PdfPTable table, Message message) {
        String from = userService.findUser(message.getFrom()).getName();
        List<PdfPCell> to = message.getTo().stream().map(userService::findUser)
                .map(User::getName).map(Phrase::new).map(PdfPCell::new).collect(Collectors.toList());
        String date = message.getData().format(DATE_TIME_FORMATTER);

        PdfPCell cell1 = new PdfPCell(new Phrase(date));
        cell1.setBackgroundColor(BaseColor.YELLOW);
        cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell1.setVerticalAlignment(Element.ALIGN_CENTER);
        cell1.setBorderWidth(1);

        PdfPCell cell2 = new PdfPCell(new Phrase(from));
        cell2.setBackgroundColor(BaseColor.YELLOW);
        cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell2.setVerticalAlignment(Element.ALIGN_CENTER);
        cell2.setBorderWidth(1);

        PdfPTable toTable = new PdfPTable(1);
        to.forEach(c -> {
            c.setBackgroundColor(BaseColor.YELLOW);
            c.setHorizontalAlignment(Element.ALIGN_CENTER);
            c.setVerticalAlignment(Element.ALIGN_CENTER);
            c.setBorderWidth(1);
        });
        to.forEach(toTable::addCell);
        PdfPCell cell3 = new PdfPCell(toTable);

        PdfPCell cell4 = new PdfPCell(new Phrase(message.getMessage()));
        cell4.setBackgroundColor(BaseColor.YELLOW);
        cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell4.setVerticalAlignment(Element.ALIGN_CENTER);
        cell4.setBorderWidth(1);
        PdfPCell cell5;
        PdfPCell cell6;

        if (message.getReply() == null) {
            cell5 = new PdfPCell(new Phrase("-"));
            cell6 = new PdfPCell(new Phrase("-"));
        } else {
            String replyFrom = userService.findUser(message.getReply().getFrom()).getName();
            cell5 = new PdfPCell(new Phrase(message.getReply().getMessage()));
            cell6 = new PdfPCell(new Phrase(replyFrom));
        }
        cell5.setBackgroundColor(BaseColor.YELLOW);
        cell5.setBorderWidth(1);
        cell6.setBackgroundColor(BaseColor.YELLOW);
        cell6.setBorderWidth(1);

        cell5.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell5.setVerticalAlignment(Element.ALIGN_CENTER);
        cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell6.setVerticalAlignment(Element.ALIGN_CENTER);

        table.addCell(cell1);
        table.addCell(cell2);
        table.addCell(cell3);
        table.addCell(cell4);
        table.addCell(cell5);
        table.addCell(cell6);
    }

    public void handleBlockUser(ActionEvent actionEvent) {
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.setTitle("Warning");

        Label warningLBL = new Label("You're about to make this friend unavailable for corresponding. Proceed?");
        warningLBL.setTextAlignment(CENTER);
        warningLBL.setWrapText(true);

        Button yesBTN = new Button("Yes");
        Button noBTN = new Button("No");

        yesBTN.setOnAction(event -> {
            if (friendshipService.findFriendshipRelations(friend.getUser(), logged) != 1L)
                MessageAlert.showErrorMessage(null, "Cannot block this friend!\nYou still can remove him as a friend!\n");
            else {
                friendshipService.updateFriend(friend.getUser(), logged, "rejected", LocalDate.now());
                myController.unloadScreen(screen3ID);
                myController.unloadScreen(screen2ID);
                myController.loadScreen(screen2ID, screen2Res, Arrays.asList(userService, friendshipService, messageService, eventService, logged));

                MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Success", "User blocked!");
                stage.close();
                myController.setScreen(screen2ID);
            }
        });
        noBTN.setOnAction(e -> stage.close());

        HBox buttonBox = new HBox(20, yesBTN, noBTN);
        buttonBox.setPadding(new Insets(0, 10, 0, 10));
        buttonBox.setAlignment(Pos.CENTER);
        VBox mainBox = new VBox(15, warningLBL, buttonBox);
        mainBox.setAlignment(Pos.CENTER);

        Scene scene = new Scene(mainBox, 250, 150);
        scene.getStylesheets().add(styling1Res);
        stage.setScene(scene);
        stage.show();
    }
}
