package socialnetwork.ui.console;

import socialnetwork.domain.Message;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.FriendshipService;
import socialnetwork.service.MessageService;
import socialnetwork.service.UserService;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static utils.Constants.DATE_TIME_FORMATTER;

public class MessageConsole extends MainConsole {
    private User logged;

    public MessageConsole(UserService userService, FriendshipService friendshipService, MessageService messageService, User logged) {
        super(userService, friendshipService, messageService);
        this.logged = logged;
    }

    @Override
    public void run() {
        while (true) {
            System.out.println("1.Send message" + newLine
                    + "2.Remove message" + newLine
                    + "3.Show conversation between you and another user" + newLine
                    + "4.Reply message" + newLine
                    + "5.Reply all to message" + newLine
                    + "6.Return to account main menu" + newLine
                    + "Choose an option from main menu by giving a number from the above." + newLine);
            int option;
            List<Long> blockingUsers = this.friendshipService.getBlockedUsers(logged, f -> f.getId().getLeft() == logged.getId())
                    .stream().map(elem -> elem.getLeft().getRight()).collect(Collectors.toList());
            try {
                option = scanner.nextInt();
                switch (option) {
                    case 6: {
                        System.out.println("Return to account main menu ...");
                        return;
                    }
                    case 5: {
                        System.out.println("Send a reply for all users for a message received!");
                        System.out.println("Reply will be given in the next form: " +
                                "id_message_to_be_replied;message_to_be_sent" + newLine);
                        String line;
                        scanner.nextLine();
                        line = scanner.nextLine();
                        List<String> attributes = Arrays.asList(line.split(";"));
                        String messageLine = "";
                        Long id = Long.parseLong(attributes.get(0));
                        String text = attributes.get(1);
                        Message message = this.messageService.findMessage(id);
                        if (message == null) {
                            throw new ConsoleException("There is no message matching ID=" + attributes.get(0) + "!");
                        } else if (!message.getTo().contains(logged.getId())) {
                            throw new ConsoleException("You can't reply a message that you did not receive!");
                        } else if (id < 1000) {
                            //transformed into id_message_to_be_replied;to_list;message_text
                            messageLine += attributes.get(0) + ";";
                            List<Long> usersTo = message.getTo();
                            usersTo.add(message.getFrom());
                            usersTo.remove(logged.getId());
                            String userIDs =
                                    usersTo.stream()
                                            .map(x -> x.toString())
                                            .reduce("", (a, b) -> a + "," + b);
                            messageLine += userIDs.substring(1) + ";" + text;
                            if (message.getReply() != null)
                                this.messageService.clearMessage(message.getReply().getId());
                            this.messageService.sendMessage(logged, messageLine, true, blockingUsers);
                        } else throw new ConsoleException("App does not permit replying another reply. " +
                                "Try sending a new message to the users instead!");
                        System.out.println("Reply sent!" + newLine);
                        break;
                    }
                    case 4: {
                        System.out.println("Send a reply to an user for a message received from him!");
                        System.out.println("Reply will be given in the next form: " +
                                "id_message_to_be_replied;message_to_be_sent");
                        System.out.println("Attention: If you a reply a message that has been replied already, the current reply will be removed!" + newLine);
                        String line;
                        scanner.nextLine();
                        line = scanner.nextLine();
                        List<String> attributes = Arrays.asList(line.split(";"));
                        String messageLine = "";
                        Long id = Long.parseLong(attributes.get(0));
                        Message message = this.messageService.findMessage(id);
                        String text = attributes.get(1);
                        if (message == null) {
                            throw new ConsoleException("There is no message matching ID=" + attributes.get(0) + "!");
                        } else if (!message.getTo().contains(logged.getId())) {
                            throw new ConsoleException("You can't reply a message that you did not receive!");
                        } else if (id < 1000) {
                            //transformed into id_message_to_be_replied;to_list;message_text
                            messageLine += attributes.get(0) + ";" + message.getFrom() + ";" + text;
                            if (message.getReply() != null)
                                this.messageService.clearMessage(message.getReply().getId());
                            this.messageService.sendMessage(logged, messageLine, true, blockingUsers);
                        } else throw new ConsoleException("App does not permit replying another reply. " +
                                "Try sending a new message to the user instead!");
                        System.out.println("Reply sent!" + newLine);
                        break;
                    }
                    case 3: {
                        System.out.println("Give an existing user to see all conversations started with him");
                        System.out.println("User will be given by his unique identifier!" + newLine);

                        Long id = scanner.nextLong();
                        System.out.println("===================Conversation=======================");
                        this.messageService.showConversation(logged, this.userService.findUser(id))
                                .forEach(m -> {
                                    System.out.println("=============================================");
                                    User sender = this.userService.findUser(m.getFrom());
                                    String from = sender.getFirstName() + " " + sender.getLastName();
                                    String to = m.getTo().stream()
                                            .map(receiver ->
                                                    this.userService.findUser(receiver).getFirstName() + " " + this.userService.findUser(receiver).getLastName())
                                            .reduce("", (x, y) -> x + ";" + y);
                                    System.out.println("From: " + from +
                                            "\nTo: " + to.substring(1) +
                                            "\nSent at: " + m.getData().format(DATE_TIME_FORMATTER) +
                                            "\nMessage: " + m.getMessage());
                                    if (m.getId() >= 1000) {
                                        Message root = this.messageService.findMessage(m.getId() / 1000);
                                        System.out.println("**************************");
                                        String details = "This was a reply for the message sent by ";
                                        details += this.userService.findUser(root.getFrom()).getFirstName() + " " + this.userService.findUser(root.getFrom()).getLastName();
                                        System.out.println(details + ". Original message was: " + root.getMessage());
                                    } else if (m.getReply() != null) {
                                        System.out.println("Message was replied with the following text: " + m.getReply().getMessage());
                                        String details = "Sent by ";//sent by (User) at (date).
                                        if (m.getReply().getFrom() == logged.getId())
                                            details += "you";
                                        else
                                            details += this.userService.findUser(m.getReply().getFrom()).getFirstName() + " " + this.userService.findUser(m.getReply().getFrom()).getLastName();
                                        details += " at " + m.getReply().getData().format(DATE_TIME_FORMATTER);
                                        System.out.println(details);
                                    } else {
                                        if (m.getFrom() != logged.getId())
                                            System.out.println("Message does not have a reply. You can reply this message with ID=" + m.getId() + "!");
                                        else System.out.println("No reply found yet!");
                                    }
                                    System.out.println("===============================================");
                                });
                        System.out.println();
                        break;
                    }
                    case 2: {
                        System.out.println("Give a message to be removed.");
                        System.out.println("Message will be given by his unique identifier");
                        System.out.println("Attention: you can only delete messages sent by you!" + newLine);

                        Long id_message = scanner.nextLong();

                        Message message = this.messageService.findMessage(id_message);
                        if (message == null)
                            throw new ConsoleException("No message matching ID=" + id_message + "!");
                        else if (message.getFrom() != logged.getId())
                            throw new ConsoleException("No message matching ID=" + id_message + "!");
                        else if (message.getReply() == null)
                            this.messageService.clearMessage(id_message);
                        else {
                            this.messageService.clearMessage(message.getReply().getId());
                            this.messageService.clearMessage(id_message);
                        }
                        System.out.println("Message deleted!" + newLine);
                        break;
                    }
                    case 1: {
                        System.out.println("Send a message to another existing user/s!");
                        System.out.println("Message will be given in the following form: ");
                        System.out.println("id_message;to1,to2,...,ton;message_to_be_sent" + newLine);

                        scanner.nextLine();
                        String messageLine = scanner.nextLine();

                        Message message = this.messageService.sendMessage(logged, messageLine, false, blockingUsers);
                        if (message != null)
                            throw new ConsoleException("Already a message in the database matching given ID!");
                        else {
                            message = this.messageService.findMessage(Long.parseLong(messageLine.substring(0, messageLine.indexOf(";"))));
                            if (message.getId() >= 1000) {
                                this.messageService.clearMessage(message.getId());
                                throw new ConsoleException("Given ID is reserved for replies only, message was not saved!");
                            }
                        }
                        System.out.println("Message sent!" + newLine);
                        break;
                    }
                }
            } catch (ConsoleException consoleException) {
                System.err.println(consoleException + newLine);
            } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
                System.err.println(indexOutOfBoundsException + newLine);
                scanner.nextLine();
            } catch (NumberFormatException numberFormatException) {
                System.err.println(numberFormatException + newLine);
                scanner.nextLine();
            } catch (ValidationException validationException) {
                System.err.println(validationException + newLine);
            }
        }
    }
}
