package socialnetwork.domain.validators;

import socialnetwork.domain.PublicEvent;

public class EventValidator implements Validator<PublicEvent> {
    @Override
    public void validate(PublicEvent entity) throws ValidationException {
        String error = "";
        if(entity.getId()<=0)
            error = error.concat("Invalid ID!\n");
        if (entity.getHost() == null)
            error = error.concat("Invalid host!\n");
        else if (entity.getHost()<=0)
            error = error.concat("Invalid host!\n");
        if (entity.getName() == null)
            error = error.concat("Invalid name of event!\n");
        else if (entity.getName().length()==0)
            error = error.concat("Invalid name of event!\n");
        if (entity.getPlace() == null)
            error = error.concat("Invalid place of event!\n");
        else if (entity.getPlace().length()==0)
            error = error.concat("Invalid place of event!\n");
        if (entity.getTime()==null)
            error = error.concat("Invalid date of the event!\n");
        if(error.length()!=0)
            throw new ValidationException(error);
    }
}
