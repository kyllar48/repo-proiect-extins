package socialnetwork.ui.console;

import socialnetwork.domain.User;
import socialnetwork.service.FriendshipService;
import socialnetwork.service.MessageService;
import socialnetwork.service.UserService;

import java.util.*;

public class MainConsole implements Console {

    protected UserService userService;
    protected FriendshipService friendshipService;
    protected MessageService messageService;
    protected String newLine = System.getProperty("line.separator");
    protected Scanner scanner = new Scanner(System.in);

    public MainConsole(UserService userService, FriendshipService friendshipService, MessageService messageService) {
        this.userService = userService;
        this.friendshipService = friendshipService;
        this.messageService = messageService;
    }

    public void run() {
        while (true) {
            int main_option;
            System.out.println("1.User Menu" + newLine
                    + "2.Account menu" + newLine
                    + "3.Community menu" + newLine
                    + "4.Close Network" + newLine
                    + "Choose an option from main menu by giving a number from the above."
                    + newLine);
            try {
                main_option = scanner.nextInt();
                switch (main_option) {
                    case 4: {
                        System.out.println("Closing network..." + newLine);
                        scanner.close();
                        return;
                    }
                    case 3: {
                        Console communityConsole = new CommunityConsole(userService,friendshipService,messageService);
                        communityConsole.run();
                        System.out.println("Community menu closed!");
                        break;
                    }
                    case 2: {
                        System.out.println("Type an existing user to proceed."
                                + "User will be given by his unique identifier: id!" + newLine);
                        Long longID = scanner.nextLong();
                        User account = this.userService.findUser(longID);
                        if (account != null) {
                            AccountSubMenu(account);
                            System.out.println("Account closed!" + newLine);
                        } else {
                            System.out.println("There are no users matching the given user!"
                                    + newLine);
                        }
                        break;
                    }
                    case 1: {
                        Console userConsole = new UserConsole(userService,friendshipService,messageService);
                        userConsole.run();
                        System.out.println("User menu closed!" + newLine);
                        break;
                    }
                    default: {
                        System.out.println("No valid option!" + newLine);
                    }
                }
            } catch (InputMismatchException inputMismatchException) {
                System.err.println("Input can only contain numbers in this menu" + newLine);
                scanner.nextLine();
            } catch (IllegalArgumentException illegalArgumentException) {
                System.err.println(illegalArgumentException + newLine);
            }
        }
    }

//    private void CommunitySubMenu() {
//        while (true) {
//            CommunityService communityService = new CommunityService(this.userService);
//            int option;
//            System.out.println("1.Number of communities" + newLine
//                    + "2.The most sociable community" + newLine
//                    + "3.Return to main menu" + newLine);
//            try {
//                option = scanner.nextInt();
//                switch (option) {
//                    case 3: {
//                        System.out.println("Closing community menu..." + newLine);
//                        return;
//                    }
//                    case 2: {
//                        System.out.println("===================Most sociable community==================");
//                        communityService.MostSociableCommunity().forEach(System.out::println);
//                        System.out.println("=================================================");
//                        break;
//                    }
//                    case 1: {
//                        System.out.println("There are "
//                                + communityService.CommunityNO()
//                                + " communities in the network!"
//                                + newLine);
//                        break;
//                    }
//                    default: {
//                        System.out.println("No valid option!" + newLine);
//                        break;
//                    }
//                }
//            } catch (InputMismatchException inputMismatchException) {
//                System.err.println("You need to give a number to proceed" + newLine);
//                scanner.nextLine();
//            }
//
//        }
//    }

//    private void UserSubMenu() {
//        while (true) {
//            int option;
//            System.out.println("1.Add User" + newLine
//                    + "2.Remove User" + newLine
//                    + "3.Update user" + newLine
//                    + "4.View users" + newLine
//                    + "5.Return to main menu" + newLine
//                    + "Choose an option from main menu by giving a number from the above."
//                    + newLine);
//            try {
//                option = scanner.nextInt();
//                switch (option) {
//                    case 5: {
//                        System.out.println("Closing user menu..." + newLine);
//                        return;
//                    }
//                    case 4: {
//                        System.out.println("===========Network users connected============");
//                        this.userService.getAll().forEach(System.out::println);
//                        System.out.println("=========================");
//                        break;
//                    }
//                    case 3: {
//                        System.out.println("Give a user to update." +
//                                "User will be given in the next form: oldID;new_firstname;new_lastname"
//                                + newLine);
//                        String line = scanner.next();
//                        User user = this.userService.updateUser(line);
//                        if (user == null)
//                            System.out.println("User updated successfully!" + newLine);
//                        else throw new ConsoleException("No user matching ID=" + user.getId() + "!" + newLine);
//                        break;
//                    }
//                    case 2: {
//                        System.out.println("Give a user to remove." +
//                                "User will be given in the next form: id;first name;last name"
//                                + newLine);
//                        String line = scanner.next();
//                        List<String> arguments = Arrays.asList(line.split(";"));
//
//                        User user = userService.findUser(Long.parseLong(arguments.get(0)));
//                        if (user != null) {
//                            System.out.println("Account removed!" + newLine);
//                            this.friendshipService.updateLists(user);
//                            this.messageService.updateMessages(user);
//                            this.userService.deleteUser(line);
//                        } else throw new ConsoleException("No account matching this infos:" + line + newLine);
//                        break;
//                    }
//                    case 1: {
//                        System.out.println("Give a user to add." +
//                                "User will be given in the next form: id;first name;last name"
//                                + newLine);
//                        String line = scanner.next();
//                        User user = this.userService.addUser(line);
//                        if (user == null)
//                            System.out.println("New account created!" + newLine);
//                        else throw new ConsoleException("Already an user with the ID=" + user.getId() + "!" + newLine);
//                        break;
//                    }
//                    default: {
//                        System.out.println("No valid option!" + newLine);
//                        break;
//                    }
//                }
//
//            } catch (ValidationException validationException) {
//                System.err.println(validationException + newLine);
//            } catch (InputMismatchException inputMismatchException) {
//                System.err.println("You need to give a number to proceed" + newLine);
//                scanner.nextLine();
//            } catch (NoSuchElementException noSuchElementException) {
//                System.err.println(noSuchElementException + newLine);
//                scanner.nextLine();
//            } catch (IllegalArgumentException illegalArgumentException) {
//                System.err.println("Cannot add to the list!" + newLine);
//            } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
//                System.err.println("Do not use spaces when you input an user" + newLine);
//                scanner.nextLine();
//            } catch (ConsoleException consoleException) {
//                System.err.println(consoleException + newLine);
//            }
//
//
//        }
//    }

//    private void FriendsSubMenu(User logged) {
//        while (true) {
//            int option;
//            System.out.println("1.Add Friend (give User)" + newLine
//                    + "2.Remove Friend (give User)" + newLine
//                    + "3.My friends" + newLine
//                    + "4.Friends added in a specific period" + newLine
//                    + "5.All my requests" + newLine
//                    + "6.Answer a request" + newLine
//                    + "7.Return to account main menu" + newLine
//                    + "Choose an option from main menu by giving a number from the above."
//                    + newLine);
//            try {
//                option = scanner.nextInt();
//                switch (option) {
//                    case 7: {
//                        System.out.println("Closing friends menu..." + newLine);
//                        return;
//                    }
//                    case 6: {
//                        System.out.println("Answering a request must have next format: id_sender;response");
//                        System.out.println("Response must be one of the following: " +
//                                "accept, reject or block." + newLine);
//                        String line = scanner.next();
//                        List<String> attributes = Arrays.asList(line.split(";"));
//                        Long sender = Long.parseLong(attributes.get(0));
//                        String response = attributes.get(1);
//                        this.friendshipService.processRequest(logged, sender, response);
//                        System.out.println();
//                        break;
//                    }
//                    case 5: {
//                        System.out.println("==================Requests===================");
//                        this.friendshipService.getRequests(logged);
//                        System.out.println("==============================================");
//                        break;
//                    }
//                    case 4: {
//                        System.out.println("Give a month and an year to search for friends.");
//                        System.out.println("Month will be given as a number from 1 to 12!" + newLine);
//                        Integer month = scanner.nextInt();
//                        System.out.println(newLine + "Year will be given as a number greater than 0!" + newLine);
//                        Integer year = scanner.nextInt();
//                        if (1 <= month && month <= 12 && year > 0) {
//                            System.out.println("===============What Search Found===================");
//                            this.friendshipService.getMyFriendsFromGivenMonth(logged, month, year)
//                                    .forEach(t ->
//                                            System.out.println(t.getLeft().getFirstName()
//                                                    + "|" + t.getLeft().getLastName() + "|" + t.getRight()));
//                            System.out.println("====================================================");
//                        } else throw new ConsoleException("Invalid month or year!" + newLine);
//                        break;
//                    }
//                    case 3: {
//                        System.out.println("===============My friends===================");
//                        this.friendshipService.getMyFriends(logged)
//                                .forEach(x -> {
//                                    System.out.println(x.getLeft().getFirstName()
//                                            + "|" + x.getLeft().getLastName() + "|" + x.getRight());
//                                });
//                        System.out.println("==============================================");
//                        break;
//                    }
//                    case 2: {
//                        System.out.println("Give a friend to remove." +
//                                "Friend will be given by his unique identifier: id!" + newLine);
//                        Long userID = scanner.nextLong();
//                        User removed = this.userService.findUser(userID);
//                        if (removed != null) {
//                            this.friendshipService.removeFriend(logged, removed);
//                            System.out.println("Friend removed successfully!" + newLine);
//                        } else throw new ConsoleException("The user to be removed currently don't exist!"
//                                + newLine);
//
//                        break;
//                    }
//                    case 1: {
//                        System.out.println("Give a friend to add." +
//                                "Friend will be given by his unique identifier: id!" + newLine);
//                        Long userID = scanner.nextLong();
//                        User added = this.userService.findUser(userID);
//                        if (added != null) {
//                            Friendship friendship = this.friendshipService.addFriend(logged, added);
//                            if (friendship == null)
//                                System.out.println("Request sent!" + newLine);
//                            else throw new ConsoleException("You already have that friend!");
//                        } else
//                            throw new ConsoleException("The user to be added currently does not exist!"
//                                    + newLine);
//                        break;
//                    }
//                    default: {
//                        System.out.println("No valid option!" + newLine);
//                        break;
//                    }
//                }
//            } catch (ValidationException validationException) {
//                System.err.println(validationException + newLine);
//            } catch (InputMismatchException inputMismatchException) {
//                System.err.println("You need to give a number to proceed" + newLine);
//                scanner.nextLine();
//            } catch (IllegalArgumentException illegalArgumentException) {
//                System.err.println(illegalArgumentException + newLine);
//            } catch (ConsoleException consoleException) {
//                System.err.println(consoleException + newLine);
//            }
//        }
//    }

    private void AccountSubMenu(User logged) {
        while (true) {
            int option;
            System.out.println("Welcome " + logged.getFirstName() + " " + logged.getLastName() + "!" + newLine
                    + "1.Friends" + newLine
                    + "2.Conversations" + newLine
                    + "3.Disconnect" + newLine
                    + "Choose an option from main menu by giving a number from the above."
                    + newLine);
            try {
                option = scanner.nextInt();
                switch (option) {
                    case 3: {
                        System.out.println("Closing account..." + newLine);
                        return;
                    }
                    case 2: {
                        Console messageConsole = new MessageConsole(userService,friendshipService,messageService,logged);
                        messageConsole.run();
                        System.out.println("Conversation menu closed!" + newLine);
                        break;
                    }
                    case 1: {
                        Console friendsConsole = new FriendsConsole(userService,friendshipService,messageService,logged);
                        friendsConsole.run();
                        System.out.println("Friends menu closed!" + newLine);
                        break;
                    }
                    default: {
                        System.out.println("No valid option!" + newLine);
                        break;
                    }
                }
            } catch (InputMismatchException inputMismatchException) {
                System.err.println("Input must only be a number" + newLine);
                scanner.nextLine();
            } catch (ConsoleException consoleException) {
                System.err.println(consoleException + newLine);
            }
        }
    }

//    private void MessageSubMenu(User logged) {
//        while (true) {
//            System.out.println("1.Send message" + newLine
//                    + "2.Remove message" + newLine
//                    + "3.Show conversation between you and another user" + newLine
//                    + "4.Reply message" + newLine
//                    + "5.Reply all to message" + newLine
//                    + "6.Return to account main menu" + newLine
//                    + "Choose an option from main menu by giving a number from the above." + newLine);
//            int option;
//            List<Long> blockingUsers = this.friendshipService.getBlockedUsers(logged,f -> f.getId().getLeft() == logged.getId())
//                    .stream().map(elem -> elem.getLeft().getRight()).collect(Collectors.toList());
//            try {
//                option = scanner.nextInt();
//                switch (option) {
//                    case 6: {
//                        System.out.println("Return to account main menu ...");
//                        return;
//                    }
//                    case 5: {
//                        System.out.println("Send a reply for all users for a message received!");
//                        System.out.println("Reply will be given in the next form: " +
//                                "id_message_to_be_replied;message_to_be_sent" + newLine);
//                        String line;
//                        scanner.nextLine();
//                        line = scanner.nextLine();
//                        List<String> attributes = Arrays.asList(line.split(";"));
//                        String messageLine = "";
//                        Long id = Long.parseLong(attributes.get(0));
//                        String text = attributes.get(1);
//                        Message message = this.messageService.findMessage(id);
//                        if (message == null) {
//                            throw new ConsoleException("There is no message matching ID=" + attributes.get(0) + "!");
//                        } else if (id < 1000) {
//                            //transformed into id_message_to_be_replied;to_list;message_text
//                            messageLine += attributes.get(0) + ";";
//                            List<Long> usersTo = message.getTo();
//                            usersTo.add(message.getFrom());
//                            usersTo.remove(logged.getId());
//                            String userIDs =
//                                    usersTo.stream()
//                                            .map(x -> x.toString())
//                                            .reduce("", (a, b) -> a + "," + b);
//                            messageLine += userIDs.substring(1) + ";" + text;
//                            if (message.getReply() != null)
//                                this.messageService.clearMessage(message.getReply().getId());
//                            this.messageService.sendMessage(logged, messageLine, true, blockingUsers);
//                        } else throw new ConsoleException("App does not permit replying another reply. " +
//                                "Try sending a new message to the users instead!");
//                        System.out.println("Reply sent!" + newLine);
//                        break;
//                    }
//                    case 4: {
//                        System.out.println("Send a reply to an user for a message received by him!");
//                        System.out.println("Reply will be given in the next form: " +
//                                "id_message_to_be_replied;message_to_be_sent");
//                        System.out.println("Attention: If you a reply a message that has been replied already, the current reply will be removed!" + newLine);
//                        String line;
//                        scanner.nextLine();
//                        line = scanner.nextLine();
//                        List<String> attributes = Arrays.asList(line.split(";"));
//                        String messageLine = "";
//                        Long id = Long.parseLong(attributes.get(0));
//                        Message message = this.messageService.findMessage(id);
//                        String text = attributes.get(1);
//                        if (message == null) {
//                            throw new ConsoleException("There is no message matching ID=" + attributes.get(0) + "!");
//                        } else if (id < 1000) {
//                            //transformed into id_message_to_be_replied;to_list;message_text
//                            messageLine += attributes.get(0) + ";" + message.getFrom() + ";" + text;
//                            if (message.getReply() != null)
//                                this.messageService.clearMessage(message.getReply().getId());
//                            this.messageService.sendMessage(logged, messageLine, true, blockingUsers);
//                        } else throw new ConsoleException("App does not permit replying another reply. " +
//                                "Try sending a new message to the user instead!");
//                        System.out.println("Reply sent!" + newLine);
//                        break;
//                    }
//                    case 3: {
//
//                        System.out.println("Give an existing user to see all conversations started with him");
//                        System.out.println("User will be given by his unique identifier!" + newLine);
//
//                        Long id = scanner.nextLong();
//                        System.out.println("===================Conversation=======================");
//                        this.messageService.showConversation(logged, this.userService.findUser(id))
//                                .forEach(m -> {
//                                    System.out.println("=============================================");
//                                    User sender = this.userService.findUser(m.getFrom());
//                                    String from = sender.getFirstName() + " " + sender.getLastName();
//                                    String to = m.getTo().stream()
//                                            .map(receiver ->
//                                                    this.userService.findUser(receiver).getFirstName() + " " + this.userService.findUser(receiver).getLastName())
//                                            .reduce("", (x, y) -> x + ";" + y);
//                                    System.out.println("From: " + from +
//                                            "\nTo: " + to.substring(1) +
//                                            "\nSent at: " + m.getData().format(DATE_TIME_FORMATTER) +
//                                            "\nMessage: " + m.getMessage());
//                                    if (m.getReply() != null) {
//                                        System.out.println("Message was replied with the following text: " + m.getReply().getMessage());
//                                        String details = "Sent by ";//sent by (User) at (date).
//                                        if (m.getReply().getFrom() == logged.getId())
//                                            details += "you";
//                                        else
//                                            details += this.userService.findUser(m.getReply().getFrom()).getFirstName() + " " + this.userService.findUser(m.getReply().getFrom()).getLastName();
//                                        details += " at " + m.getReply().getData().format(DATE_TIME_FORMATTER);
//                                        System.out.println(details);
//                                    } else {
//                                        if (m.getFrom() != logged.getId())
//                                            System.out.println("Message does not have a reply. You can reply this message with ID=" + m.getId() + "!");
//                                        else System.out.println("No reply found yet!");
//                                    }
//                                    System.out.println("===============================================");
//                                });
//                        System.out.println();
//                        break;
//                    }
//                    case 2: {
//                        System.out.println("Give a message to be removed.");
//                        System.out.println("Message will be given by his unique identifier");
//                        System.out.println("Attention: you can only delete messages sent by you!" + newLine);
//
//                        Long id_message = scanner.nextLong();
//
//                        Message message = this.messageService.findMessage(id_message);
//                        if (message == null)
//                            throw new ConsoleException("No message matching ID=" + id_message + "!");
//                        else if (message.getFrom() != logged.getId())
//                            throw new ConsoleException("No message matching ID=" + id_message + "!");
//                        else if (message.getReply() == null)
//                            this.messageService.clearMessage(id_message);
//                        else {
//                            this.messageService.clearMessage(message.getReply().getId());
//                            this.messageService.clearMessage(id_message);
//                        }
//                        System.out.println("Message deleted!" + newLine);
//                        break;
//                    }
//                    case 1: {
//                        System.out.println("Send a message to another existing user/s!");
//                        System.out.println("Message will be given in the following form: ");
//                        System.out.println("id_message;to1,to2,...,ton;message_to_be_sent" + newLine);
//
//                        scanner.nextLine();
//                        String messageLine = scanner.nextLine();
//
//                        Message message = this.messageService.sendMessage(logged, messageLine, false, blockingUsers);
//                        if (message != null)
//                            throw new ConsoleException("Already a message in the database matching given ID!");
//                        else {
//                            message = this.messageService.findMessage(Long.parseLong(messageLine.substring(0, messageLine.indexOf(";"))));
//                            if (message.getId() > 1000) {
//                                this.messageService.clearMessage(message.getId());
//                                throw new ConsoleException("Given ID is reserved for replies only!");
//                            } else {
//
//
//                            }
//                        }
//                        System.out.println("Message sent!" + newLine);
//                        break;
//                    }
//                }
//            } catch (ConsoleException consoleException) {
//                System.err.println(consoleException + newLine);
//            } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
//                System.err.println(indexOutOfBoundsException + newLine);
//                scanner.nextLine();
//            } catch (NumberFormatException numberFormatException) {
//                System.err.println(numberFormatException + newLine);
//                scanner.nextLine();
//            } catch (ValidationException validationException) {
//                System.err.println(validationException + newLine);
//            }
//        }
//    }

}