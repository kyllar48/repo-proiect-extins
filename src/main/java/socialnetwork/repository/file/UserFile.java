package socialnetwork.repository.file;

import socialnetwork.domain.User;
import socialnetwork.domain.validators.Validator;

import java.util.List;

public class UserFile extends AbstractFileRepository<java.lang.Long, User>{

    public UserFile(String fileName, Validator<User> validator) {
        super(fileName, validator);
    }

    /**
     * extract Entity - method for extracting an user from a list of attributes
     * @param attributes
     *          must not be null
     *          An user will be extracted from this list
     * @return  A 'brute' user, ready to be validated.
     */
    @Override
    public User extractEntity(List<String> attributes) {
        User user = new User(attributes.get(1),attributes.get(2),attributes.get(3));
        user.setId(Long.parseLong(attributes.get(0)));
        return user;
    }

    /**
     * create entity as string - method for encoding an user to string
     * @param entity
     *          must not be null
     *          must be an user
     * @return a string which represent an user.
     */
    @Override
    protected String createEntityAsString(User entity) {
        return entity.getId()+";"+entity.getFirstName()+";"+entity.getLastName();
    }
}
