package utils;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.ListView;
import javafx.scene.control.Pagination;
import javafx.scene.control.TableView;
import javafx.util.Callback;

public class PaginatorUtils {

    public static final Integer ROWS_LIST = 5;
    public static final Integer ROWS_TABLE = 6;

    public static void getTablePager(Pagination pagination, TableView tableView, ObservableList tableList) {
        if (tableList.size() % ROWS_TABLE != 0)
            pagination.setPageCount(tableList.size() / ROWS_TABLE + 1);
        else pagination.setPageCount(tableList.size() / ROWS_TABLE);
        if (tableList.size() != 0) {
            pagination.setVisible(true);
            tableView.setVisible(true);
            pagination.setPageFactory(new Callback<Integer, Node>() {
                @Override
                public Node call(Integer pageIndex) {

                    Integer tableListSize = tableList.size();

                    if (pageIndex > tableListSize / ROWS_TABLE + 1) {
                        return null;
                    }
                    int lastIndex;
                    int displace = tableListSize % ROWS_TABLE;
                    if (displace > 0)
                        lastIndex = tableListSize / ROWS_TABLE;
                    else {
                        lastIndex = tableListSize / ROWS_TABLE - 1;
                        displace = ROWS_TABLE;
                    }

                    if (lastIndex == pageIndex)
                        tableView.setItems(FXCollections.observableArrayList(tableList.subList(pageIndex * ROWS_TABLE, pageIndex * ROWS_TABLE + displace)));
                    else
                        tableView.setItems(FXCollections.observableArrayList(tableList.subList(pageIndex * ROWS_TABLE, pageIndex * ROWS_TABLE + ROWS_TABLE)));

                    return tableView;
                }
            });
        } else {
            pagination.setVisible(false);
            tableView.setVisible(false);
        }
    }

    public static void getListPager(Pagination pagination, ListView listView, ObservableList listList) {
        if (listList.size() % ROWS_LIST != 0)
            pagination.setPageCount(listList.size() / ROWS_LIST + 1);
        else pagination.setPageCount(listList.size() / ROWS_LIST);
        if (listList.size() != 0) {
            pagination.setVisible(true);
            listView.setVisible(true);
            pagination.setPageFactory(new Callback<Integer, Node>() {
                @Override
                public Node call(Integer pageIndex) {

                    Integer listListSize = listList.size();

                    if (pageIndex > listListSize / ROWS_LIST + 1) {
                        return null;
                    }
                    int lastIndex;
                    int displace = listListSize % ROWS_LIST;
                    if (displace > 0)
                        lastIndex = listListSize / ROWS_LIST;
                    else {
                        lastIndex = listListSize / ROWS_LIST - 1;
                        displace = ROWS_LIST;
                    }
                    if (lastIndex == pageIndex)
                        listView.setItems(FXCollections.observableArrayList(listList.subList(pageIndex * ROWS_LIST, pageIndex * ROWS_LIST + displace)));
                    else
                        listView.setItems(FXCollections.observableArrayList(listList.subList(pageIndex * ROWS_LIST, (pageIndex + 1) * ROWS_LIST)));

                    return listView;
                }
            });
        } else {
            pagination.setVisible(false);
            listView.setVisible(false);
        }
    }
}
