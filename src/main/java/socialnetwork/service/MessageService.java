package socialnetwork.service;

import socialnetwork.domain.Message;
import socialnetwork.domain.User;
import socialnetwork.repository.Repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class MessageService {
    private Repository<Long, Message> messageRepo;

    public MessageService(Repository<Long, Message> messageRepo) {
        this.messageRepo = messageRepo;
    }


    private Message extractEntity(User logged, String line, Boolean isReply, List<Long> toRemove) {
        List<String> attributes = Arrays.asList(line.split(";"));
        List<Long> toIDs = Arrays.stream(attributes.get(1).split(","))
                .map(Long::parseLong).collect(Collectors.toList());
        Message message = new Message(logged.getId(), toIDs, attributes.get(2), LocalDateTime.now());
        if (isReply)
            message.setId(Long.parseLong(attributes.get(0)) * 1000);
        else message.setId(Long.parseLong(attributes.get(0)));
        toRemove.forEach(message::removeMessageReceiver);
        return message;
    }

    //Un add message
    public Message sendMessage(User logged, String messageLine, Boolean isReply, List<Long> toRemove) {
        Message message = this.messageRepo.save(extractEntity(logged, messageLine, isReply, toRemove));
        if (isReply && message == null) {
            List<String> attr = Arrays.asList(messageLine.split(";"));
            Message message1 = findMessage(Long.parseLong(attr.get(0))*1000);
            Long id = message1.getId() / 1000;
            findMessage(id).setReply(message1);
        }
        return message;
    }

    //Un remove message
    public Message clearMessage(Long id) {
        Message message = this.messageRepo.delete(id);
        if(message!=null)
            if(message.getId()>=1000)
                findMessage(id/1000).setReply(null);
        return message;
    }

    public List<Message> showConversation(User logged, User correspondent) {
        Predicate<Message> isSentByLogged = m -> m.getId() < 1000 && m.getFrom() == logged.getId() && m.getTo().contains(correspondent.getId());
        Predicate<Message> isReceivedByLogged = m -> m.getId() < 1000 && m.getTo().contains(logged.getId()) && m.getFrom() == correspondent.getId();
        Predicate<Message> isReplyToAll = m -> m.getId() >= 1000 && m.getTo().contains(logged.getId()) && m.getTo().contains(correspondent.getId());
        List<Message> messageList = new ArrayList<>();
        this.messageRepo.findAll().forEach(messageList::add);
        messageList = messageList.stream()
                .filter(isSentByLogged.or(isReceivedByLogged).or(isReplyToAll))
                .sorted((o1, o2) -> {
                    if (o1.getData().isBefore(o2.getData()))
                        return -1;
                    else if (o1.getData().isEqual(o2.getData()))
                        return 0;
                    return 1;
                }).collect(Collectors.toList());
        List<Message> bruteMessageList = messageList;
        Predicate<Message> replyWithRootMsgInList = m -> m.getId() >= 1000 && bruteMessageList.contains(findMessage(m.getId() / 1000));
        messageList.removeIf(replyWithRootMsgInList);
        return messageList;
    }

    public Message findMessage(Long id) {
        return this.messageRepo.findOne(id);
    }

    public void updateMessages(User removed) {
        List<Message> messages = new ArrayList<>();
        this.messageRepo.findAll().forEach(messages::add);
        messages.forEach(m -> {
            if (m.getFrom() == removed.getId()) {
                if (m.getReply() != null)
                    this.messageRepo.delete(m.getReply().getId());
                this.messageRepo.delete(m.getId());
            } else if (m.getTo().contains(removed.getId())) {
                if (m.getReply() != null)
                    this.messageRepo.delete(m.getReply().getId());
                this.messageRepo.delete(m.getId());
                m.removeMessageReceiver(removed.getId());
                this.messageRepo.save(m);
                if (m.getReply() != null) {
                    m.getReply().removeMessageReceiver(removed.getId());
                    this.messageRepo.save(m.getReply());
                }
            }
        });
    }

    public Long getAvailableID() {
        List<Long> messageIDs = new ArrayList<>();
        this.messageRepo.findAll().forEach(m -> messageIDs.add(m.getId()));
        messageIDs.sort(new Comparator<Long>() {
            @Override
            public int compare(Long o1, Long o2) {
                if (o1 == o2) return 0;
                if (o1 < o2) return -1;
                return 1;
            }
        });
        if (messageIDs.size() == 0)
            return 1l;
        else if (messageIDs.get(0) > 1)
            return messageIDs.get(0) - 1;
        Long returnedID = messageIDs.get(0);
        for (Long id : messageIDs)
            if (id == returnedID)
                returnedID++;
            else break;
        return returnedID;
    }

    public List<Message> getMyMessages(User logged){
        Predicate<Message> isMyMessage = m -> m.getFrom() == logged.getId();
        List<Message> list = new ArrayList<>();
        this.messageRepo.findAll().forEach(list::add);
        return list.stream().filter(isMyMessage).collect(Collectors.toList());
    }

}
