package socialnetwork.domain.validators;

import socialnetwork.domain.Friendship;
import socialnetwork.domain.Tuple;

/**
 * Friendship validator class
 */
public class FriendshipValidator implements Validator<Friendship>{
    /**
     *
     * @param entity - the object to be validated.
     * @throws ValidationException - if one or more entity's informations are missing or a user
     * tries to friend himself.
     */
    @Override
    public void validate(Friendship entity) throws ValidationException {
        Tuple<Long, Long> friends=entity.getId();
        if(friends.getLeft()==null && friends.getRight()==null)
            throw new ValidationException("There are no users with the given identificators!");
        if(friends.getLeft()==null)
            throw new ValidationException("There are no users matching ID=" +
                    friends.getLeft() + " in the network!");
        if(friends.getRight()==null)
            throw new ValidationException("There are no users matching ID=" +
                    friends.getRight() + " in the network!");
        if(friends.getLeft()==friends.getRight())
            throw new ValidationException("No valid friendship, you can't friend yourself here :)");
        if(friends.getLeft()<=0 || friends.getRight()<=0)
            throw new ValidationException("ID of a friend must not be null or negative!");
    }
}
