package socialnetwork.domain;

import java.time.LocalDate;


public class Friendship extends Entity<Tuple<Long, Long>> {

    LocalDate date;
    String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Friendship() {
    }

    /**
     *
     * @return the date when the friendship was created
     */
    public LocalDate getDate() {
        return date;
    }


    @Override
    public String toString() {
        return "Friendship{" +
                " Friends=" + this.getId() +
                " date=" + date +
                '}';
    }
}
