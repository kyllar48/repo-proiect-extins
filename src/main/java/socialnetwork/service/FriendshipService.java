package socialnetwork.service;

import socialnetwork.domain.Friendship;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.User;
import socialnetwork.repository.Repository;
import socialnetwork.service.wrappers.FriendRequest;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static utils.Constants.DATE_FORMATTER;

/**
 * FriendshipService class.
 * Class used to incorporate all the important aspects of
 * managing a list of friendships.
 */
public class FriendshipService {
    private Repository<Tuple<Long, Long>, Friendship> frepo;
    private UserService userService;

    public FriendshipService(Repository<Tuple<Long, Long>, Friendship> frepo, UserService userService) {
        this.frepo = frepo;
        this.userService = userService;
        loadFriendsList();
    }

    /**
     * Method to load friends lists
     * Used in constructor to save the friendships added prior the running of the program.
     * (e.g. if the data was in a text file).
     */
    private void loadFriendsList() {
        this.frepo.findAll().forEach(
                (Friendship f) -> {
                    if (f.getStatus().equals("approved")) {
                        User user1 = this.userService.findUser(f.getId().getLeft());
                        User user2 = this.userService.findUser(f.getId().getRight());
                        user1 = user1.saveFriend(f.getId().getRight());
                        user2 = user2.saveFriend(f.getId().getLeft());
                    }
                });
    }

    /**
     * Method to decode a friendship from two users provided and an encoded date
     *
     * @param logged   - user who manages actions with friends.
     * @param toChange - user managed to be changed by user logged.
     * @param dateTime - a string containing the date when the online friendship started.
     * @return a 'brute',non-validated friendship.
     */
    private Friendship extractFriendship(User logged, User toChange, String dateTime) {
        Friendship friendship = new Friendship();
        friendship.setId(new Tuple<>(logged.getId(), toChange.getId()));
        friendship.setDate(LocalDate.parse(dateTime, DATE_FORMATTER));
        friendship.setStatus("pending");
        return friendship;
    }

    /**
     * Method to add a new friendship into the friendship's lists in the program.
     *
     * @param logged - user adding a new friend.
     * @param added  - user to be added as friend.
     * @return null if the given friendship was saved;
     * the given friendship, otherwise.
     */
    public Friendship addFriend(User logged, User added) {
        Friendship f = frepo.save(extractFriendship(logged, added,
                LocalDate.now().format(DATE_FORMATTER)));
        return f;
    }

    /**
     * Method to obtain a list of users who are friends with a given user
     * and the dates when the friendships started.
     *
     * @param logged - user who wants to know their friends.
     * @return friends - the searched list.
     */
    public List<Tuple<User, LocalDate>> getMyFriends(User logged) {
        Predicate<Friendship> isAccepted = f -> f.getStatus().equals("approved");
        Predicate<Friendship> isLeft = f -> f.getId().getLeft() == logged.getId();
        Predicate<Friendship> isRight = f -> f.getId().getRight() == logged.getId();
        List<Friendship> friendshipList = new ArrayList<>();
        this.frepo.findAll().forEach(friendshipList::add);
        return friendshipList.stream()
                .filter(isAccepted.and(isLeft.or(isRight)))
                .map(
                        f -> new Tuple<>(this.userService.findUser(f.getId().getLeft() + f.getId().getRight() - logged.getId()), f.getDate()))
                .collect(Collectors.toList());
    }

    private void insertInFriendsList(User logged, User added) {
        userService.getAll().forEach(x ->
        {
            if (x == logged)
                x = x.saveFriend(added.getId());
            if (x == added)
                x = x.saveFriend(logged.getId());
        });
    }

    public List<Tuple<User, LocalDate>> getMyFriendsFromGivenMonth(User logged, Integer month, Integer year) {
        Predicate<Tuple<User, LocalDate>> isGivenMonth = t -> t.getRight().getMonthValue() == month;
        Predicate<Tuple<User, LocalDate>> isGivenYear = t -> t.getRight().getYear() == year;
        return getMyFriends(logged).stream()
                .filter(isGivenMonth.and(isGivenYear)).collect(Collectors.toList());
    }

    public void getRequests(User logged) {
        this.frepo.findAll().forEach((Friendship f) -> {
            if (f.getStatus().equals("pending")) {
                User user;
                if (logged.getId() == f.getId().getRight()) {
                    user = this.userService.findUser(f.getId().getLeft());
                    System.out.println(user.getId() + "|" + user.getFirstName() + " " + user.getLastName() + "|Request received");
                } else if (logged.getId() == f.getId().getLeft()) {
                    user = this.userService.findUser(f.getId().getRight());
                    System.out.println(user.getId() + "|" + user.getFirstName() + " " + user.getLastName() + "|Request sent");
                }
            }
        });
    }

    public void processRequest(User logged, Long id_sender, String response) {
        Tuple<Long, Long> searched = new Tuple<>(id_sender, logged.getId());
        Friendship f = frepo.findOne(searched);
        if (f == null)
            System.out.println("No request received matching ID=" + id_sender + "!");
        else if (f.getStatus().equals("approved"))
            System.out.println("Already friends!");
        else if (f.getStatus().equals("pending"))
            if (response.equals("accept")) {
                f.setStatus("approved");
                f.setDate(LocalDate.now());
                insertInFriendsList(logged, userService.findUser(id_sender));
                this.frepo.update(f);
                System.out.println("Friend added!");
            } else if (response.equals("reject")) {
                frepo.delete(searched);
                System.out.println("Request rejected!");
            } else if (response.equals("block")) {
                f.setStatus("rejected");
                f.setDate(LocalDate.now());
                this.frepo.update(f);
                System.out.println("User blocked!");
            } else System.out.println("You can only type accept or reject as response!");

    }

    /**
     * Method to remove a friendship from the friendship's lists in the program.
     *
     * @param logged  - user removing a friend.
     * @param removed - an user removed by the user logged.
     * @return null if there's no friendship matchin the given informations.
     * removed, otherwise.
     */
    public Friendship removeFriend(User logged, User removed) {
        Friendship f = frepo.delete(extractFriendship(logged, removed, "2020-10-28").getId());
        userService.getAll().forEach((User user) ->
        {
            if (user == logged)
                user = user.removeFriend(removed.getId());
            if (user == removed)
                user = user.removeFriend(logged.getId());
        });
        return f;
    }

    /**
     * Method to update friend's list of all users,
     * if a given user will be removed from the program
     *
     * @param user - an user that will be removed from program by some other methods.
     */
    public void updateLists(User user) {
        Long removedId = user.getId();
        user.getFriends().forEach((Long id) -> {
            User toUpdate = this.userService.findUser(id);
            toUpdate = toUpdate.removeFriend(removedId);
        });
        List<Tuple<Long, Long>> unnecesaryFriendships = new ArrayList<>();
        this.frepo.findAll().forEach((Friendship f) -> {
            if (f.getId().getLeft() == removedId || f.getId().getRight() == removedId)
                unnecesaryFriendships.add(f.getId());
        });
        unnecesaryFriendships.forEach(friendshipID -> {
            this.frepo.delete(friendshipID);
        });
    }

    public List<Tuple<Tuple<Long, Long>, LocalDate>> getBlockedUsers(User logged, Predicate<Friendship> perspective) {
        Predicate<Friendship> isBlocked = f -> f.getStatus().equals("rejected");
        //Predicate<Friendship> isLogged = f -> f.getId().getRight() == logged.getId();
        List<Friendship> friendships = new ArrayList<>();
        this.frepo.findAll().forEach(friendships::add);
        return friendships.stream().filter(isBlocked.and(perspective))
                .map(f -> new Tuple<>(f.getId(), f.getDate())).collect(Collectors.toList());
    }

    public Friendship unblockUser(User logged, Long id) {
        Friendship friendship = this.frepo.findOne(new Tuple<>(id, logged.getId()));
        if (friendship != null)
            if (friendship.getStatus().equals("rejected")) {
                friendship = this.frepo.delete(friendship.getId());
            } else friendship = null;
        else ;
        return friendship;
    }

    public Friendship findFriendship(Tuple<Long, Long> id) {
        return this.frepo.findOne(id);
    }

    public List<User> findUsersThatAreNotMyFriends(User logged) {
        Predicate<User> isNotFriendOrRequest = u ->
                this.frepo.findOne(new Tuple<>(u.getId(), logged.getId())) == null && this.frepo.findOne(new Tuple<>(logged.getId(), u.getId())) == null;
        Predicate<User> isLogged = u -> u.getId() == logged.getId();
        List<User> allUsers = new ArrayList<>();
        this.userService.getAll().forEach(allUsers::add);
        return allUsers.stream().filter(isNotFriendOrRequest.and(isLogged.negate())).collect(Collectors.toList());
    }

    public List<FriendRequest> getAllRequests(User logged, Predicate<Friendship> condition) {
        List<Friendship> allFriends = new ArrayList<>();
        this.frepo.findAll().forEach(allFriends::add);


        return allFriends.stream().filter(condition).map(f -> {
            return new FriendRequest(this.userService.findUser(f.getId().getLeft() + f.getId().getRight() - logged.getId()), f.getDate(), f.getStatus());
        }).collect(Collectors.toList());


    }

    public long findFriendshipRelations(User logged, User ot) {
        Friendship f = frepo.findOne(new Tuple<>(logged.getId(), ot.getId()));
        if (f == null)
            return frepo.findOne(new Tuple<>(ot.getId(), logged.getId())) == null ? 0L : -1L;
        return 1L;
    }

    public void updateFriend(User logged, User ot, String status, LocalDate date) {
        Friendship f = frepo.findOne(new Tuple<>(logged.getId(), ot.getId()));
        f.setDate(date);
        f.setStatus(status);
        frepo.update(f);
    }

}
