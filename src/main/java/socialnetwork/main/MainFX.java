package socialnetwork.main;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import socialnetwork.domain.*;
import socialnetwork.domain.validators.EventValidator;
import socialnetwork.domain.validators.FriendshipValidator;
import socialnetwork.domain.validators.MessageValidator;
import socialnetwork.domain.validators.UserValidator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.EventDB;
import socialnetwork.repository.database.FriendshipDB;
import socialnetwork.repository.database.MessageDB;
import socialnetwork.repository.database.UserDB;
import socialnetwork.service.EventService;
import socialnetwork.service.FriendshipService;
import socialnetwork.service.MessageService;
import socialnetwork.service.UserService;
import socialnetwork.ui.gui.ScreensController;

import java.util.ArrayList;
import java.util.List;

import static utils.Constants.*;

public class MainFX extends Application {



    @Override
    public void start(Stage primaryStage) throws Exception{

        ScreensController controller = new ScreensController();

        UserService userService = getUserService();

        List<Object> objectList = new ArrayList<>();
        objectList.add(userService);
        objectList.add(getFriendService(userService));
        objectList.add(getMsgService());
        objectList.add(getEventService());

        controller.loadScreen(screen1ID,screen1Res,objectList);

        controller.setScreen(screen1ID);


        AnchorPane root = new AnchorPane();
        root.getChildren().addAll(controller);
        Scene scene = new Scene(root);
        scene.getStylesheets().add(styling1Res);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Social Network");
        primaryStage.setResizable(false);
        primaryStage.show();

    }

    private EventService getEventService() {
        Repository<Long, PublicEvent> eventRepository = new EventDB(new EventValidator(),URL);
        return new EventService(eventRepository);
    }

    public static void main(String[] args) {
        launch(args);
    }

    private UserService getUserService(){
        Repository<Long, User> userDataBaseRepository=new UserDB(new UserValidator(),URL);
        return new UserService(userDataBaseRepository);
    }

    private FriendshipService getFriendService(UserService userService){
        Repository<Long,User> userDataBaseRepository=new UserDB(new UserValidator(),URL);
        Repository<Tuple<Long,Long>,Friendship> friendshipDataBaseRepository=new FriendshipDB(new FriendshipValidator(),URL);

        return new FriendshipService(friendshipDataBaseRepository, userService);

    }

    private MessageService getMsgService(){
        Repository<Long, Message> messageDatBaseRepository=new MessageDB(new MessageValidator(), URL);
        return new MessageService(messageDatBaseRepository);
    }
}
