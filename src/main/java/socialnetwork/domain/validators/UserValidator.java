package socialnetwork.domain.validators;

import socialnetwork.domain.User;

/**
 * User validator class.
 */
public class UserValidator implements Validator<User> {
    /**
     *
     * @param entity - the object to be validated.
     * @throws ValidationException -
     * if one or more informations regarding the entity's firstname/lastname are
     * missing/not valid from a name's perspective.
     */
    @Override
    public void validate(User entity) throws ValidationException {
        if(entity.getFirstName()==null && entity.getLastName()==null)
            throw new ValidationException("Name informations were missing!");
        if(entity.getFirstName()==null)
            throw new ValidationException("First name was missing!");
        else if(entity.getFirstName().equals(""))
            throw new ValidationException("First name was missing!");
        if(entity.getLastName()==null)
            throw new ValidationException("Last name was missing!");
        else if(entity.getLastName().equals(""))
            throw new ValidationException("Last name was missing!");
        if(!entity.getFirstName().matches("^[A-Z][a-z]{2,}[\\-]?([A-Z][a-z]{2,})?$"))
            throw new ValidationException("First name not valid!");
        if(!entity.getLastName().matches("^[A-Z][a-z]{2,}[\\-]?([A-Z][a-z]{2,})?$"))
            throw new ValidationException("Last name not valid!");
        if(entity.getId()<=0)
            throw new ValidationException("ID must not be null or negative!");

    }
}
