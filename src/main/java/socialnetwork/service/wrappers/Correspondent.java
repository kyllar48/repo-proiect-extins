package socialnetwork.service.wrappers;

import javafx.scene.control.CheckBox;
import socialnetwork.domain.User;

public class Correspondent {
    private User user;
    private String name;
    private CheckBox selector;

    public Correspondent(User user) {
        this.user = user;
        this.name= user.getName();
        this.selector = new CheckBox();
        this.selector.setSelected(false);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CheckBox getSelector() {
        return selector;
    }

    public void setSelector(Boolean value) {
        this.selector.setSelected(value);
    }
}
