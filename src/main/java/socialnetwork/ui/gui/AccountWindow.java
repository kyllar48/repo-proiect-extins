package socialnetwork.ui.gui;

import com.itextpdf.text.Rectangle;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPHeaderCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.util.converter.LocalTimeStringConverter;
import socialnetwork.domain.Friendship;
import socialnetwork.domain.Message;
import socialnetwork.domain.PublicEvent;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.EventService;
import socialnetwork.service.FriendshipService;
import socialnetwork.service.MessageService;
import socialnetwork.service.UserService;
import socialnetwork.service.wrappers.Activity;
import socialnetwork.service.wrappers.Correspondent;
import socialnetwork.service.wrappers.ExternUser;
import socialnetwork.service.wrappers.FriendRequest;
import utils.PaginatorUtils;
import utils.PasswordUtils;

import java.awt.*;
import java.io.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static utils.Constants.*;

public class AccountWindow implements Screen {

    private User logged;
    private UserService userService;
    private FriendshipService friendshipService;
    private MessageService messageService;
    private EventService eventService;
    private ScreensController myController;

    private List<Long> blockedUsers;

    ObservableList<ExternUser> friendships = FXCollections.observableArrayList();
    ObservableList<User> nonFriends = FXCollections.observableArrayList();


    ObservableList<FriendRequest> sentRequests = FXCollections.observableArrayList();
    ObservableList<FriendRequest> receivedRequests = FXCollections.observableArrayList();

    @FXML
    TextField usersNameFilter;


    private void initializeInterface() {
        updateMyPage();
        updateFriendsTab();
        updateRequestsTab();
        updateMessageTab();
        updateEventsTab();
    }

    @FXML
    Label nameLBL;
    @FXML
    Label usernameLBL;
    @FXML
    ImageView profileIMG;

    private void updateMyPage() {
        nameLBL.setText("Name: " + logged.getName());
        usernameLBL.setText("Username: " + logged.getUserName());
        FileInputStream input = null;
        try {
            input = new FileInputStream(PICS_PATH + logged.getUserName() + ".jpg");
        } catch (Exception e) {
            try {
                input = new FileInputStream(PICS_PATH + "unknown.jpg");
            } catch (FileNotFoundException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }
        }
        profileIMG.setImage(new Image(input));
    }

    ObservableList<PublicEvent> events = FXCollections.observableArrayList();

    @FXML
    ListView<PublicEvent> eventsList;
    @FXML
    ToggleButton eventsButton;
    @FXML
    Tab eventsTab;
    @FXML
    Pagination eventsPaginator;

    public void updateEventsTabText() {
        Long incommingEvents = this.eventService.getImminentEventsNO(logged.getId());
        if (incommingEvents != 0)
            eventsTab.setText("Events (" + incommingEvents + " incoming)");
        else eventsTab.setText("Events");
    }

    private void editEventsCells() {
        eventsList.setCellFactory(list -> new ListCell<PublicEvent>() {
            @Override
            protected void updateItem(PublicEvent item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null || empty)
                    setText(null);
                else if (logged.getId() == item.getHost() || item.getAttendants().contains(logged.getId())) {
                    Long imminency = item.getIminencyRating(logged.getId());
                    if (imminency == -1L)
                        setText(item.getName() + " => 1 day ago");
                    else if (imminency < 0L)
                        setText(item.getName() + " => " + -imminency + " days ago");
                    else if (imminency == 0L)
                        if (item.getTime().getDayOfYear() == LocalDate.now().getDayOfYear())
                            setText(item.getName() + " => today at " + item.getTime().format(TIME_FORMATTER));
                        else setText(item.getName() + " => tommorow at " + item.getTime().format(TIME_FORMATTER));
                    else if (imminency == 1L)
                        setText(item.getName() + " => incoming (" + imminency + " day remaining)");
                    else if (imminency <= IMMINENT_PERIOD)
                        setText(item.getName() + " => incoming (" + imminency + " days remaining)");
                    else setText(item.getName() + " => (you'll be notified in the time of the event)");
                } else setText(item.getName());
            }
        });
    }

    private void updateEventsTab() {
        events.setAll(this.eventService.getExternEvents(this.logged.getId()));


        editEventsCells();
        PaginatorUtils.getListPager(eventsPaginator, eventsList, events);
//        eventsList.setItems(events);

        updateEventsTabText();
    }

    public void eventsChoiceHandler(ActionEvent actionEvent) {
        if (eventsButton.isSelected()) {
            events.setAll(this.eventService.getMyEvents(this.logged.getId()));
            eventsButton.setText("View Other Events");
        } else {
            events.setAll(this.eventService.getExternEvents(this.logged.getId()));
            eventsButton.setText("View My Events");
        }
        PaginatorUtils.getListPager(eventsPaginator, eventsList, events);
    }

    public void eventCreatorHandler(ActionEvent actionEvent) {
        Label placeLBL = new Label("Place");
        Label nameLBL = new Label("Event");
        Label timeLBL = new Label("Date");

        TextField placeINFO = new TextField();
        TextField nameINFO = new TextField();

        SpinnerValueFactory<LocalTime> value = new SpinnerValueFactory<LocalTime>() {
            {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
                setConverter(new LocalTimeStringConverter(formatter, null));
            }

            @Override
            public void decrement(int steps) {
                if (getValue() == null)
                    setValue(LocalTime.of(12, 0));
                else {
                    LocalTime time = getValue();
                    setValue(time.minusMinutes(15));
                }
            }

            @Override
            public void increment(int steps) {
                if (this.getValue() == null)
                    setValue(LocalTime.of(12, 0));
                else {
                    LocalTime time = getValue();
                    setValue(time.plusMinutes(15));
                }
            }
        };
        Spinner<LocalTime> timeSpinner = new Spinner<>(value);
        DatePicker datePicker = new DatePicker();
        HBox timeINFO = new HBox(10, datePicker, timeSpinner);


        GridPane infoPane = new GridPane();

        infoPane.addRow(0, nameLBL, nameINFO);
        infoPane.addRow(1, placeLBL, placeINFO);
        infoPane.addRow(2, timeLBL, timeINFO);

        infoPane.setHgap(15);
        infoPane.setVgap(15);
        infoPane.setAlignment(Pos.CENTER);

        Stage stage = new Stage();
        stage.setTitle("Create Event Panel");
        stage.setResizable(false);
        stage.initModality(Modality.APPLICATION_MODAL);

        Button cancelButton = new Button("Cancel");
        cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                stage.close();
            }
        });
        Button addButton = new Button("Submit");
        addButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String name = nameINFO.getText().strip();
                String place = placeINFO.getText().strip();
                LocalDate date = datePicker.getValue();
                LocalTime time = timeSpinner.getValue();
                try {
                    eventService.addEvent(logged.getId(), name, place, date, time);
                    if (eventsButton.isSelected()) {
                        events.setAll(eventService.getMyEvents(logged.getId()));
                        PaginatorUtils.getListPager(eventsPaginator, eventsList, events);
                    }
                    MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Success", "Event added succesfully!");
                    stage.close();
                    updateEventsTabText();
                } catch (ValidationException v) {
                    MessageAlert.showErrorMessage(null, v.getMessage());
                } catch (Exception e) {
                    MessageAlert.showErrorMessage(null, "Invalid date or time of event!");
                } finally {
                    nameINFO.clear();
                    placeINFO.clear();
                    datePicker.setValue(null);
                    timeSpinner.getValueFactory().setValue(null);
                    editEventsCells();
                }
            }
        });
        cancelButton.setPrefSize(100, 30);
        addButton.setPrefSize(100, 30);

        HBox buttonBox = new HBox(30, addButton, cancelButton);
        buttonBox.setAlignment(Pos.CENTER);

        VBox mainBox = new VBox(20, infoPane, buttonBox);
        mainBox.setAlignment(Pos.CENTER);

        Scene scene = new Scene(mainBox, 500, 250);
        scene.getStylesheets().add(styling1Res);
        stage.setScene(scene);

        stage.showAndWait();
    }

    public void selectEventHandler(MouseEvent mouseEvent) {
        try {
            PublicEvent publicEvent = eventsList.getSelectionModel().getSelectedItem();
            if (publicEvent.getHost() == logged.getId())
                myEventHandler(publicEvent);
            else externEventHandler(publicEvent);
        } catch (NullPointerException n) {
        }
    }

    private void externEventHandler(PublicEvent publicEvent) {
        Label hostLBL = new Label("Hosted by");
        Label attendantsLBL = new Label("Attendants");
        Label placeLBL = new Label("Place");
        Label nameLBL = new Label("Event");
        Label timeLBL = new Label("Date");

        Label placeINFO = new Label(publicEvent.getPlace());
        Label nameINFO = new Label(publicEvent.getName());
        Label hostINFO = new Label(publicEvent.getHost(this.userService).getName());
        ObservableList<User> attendantsList = FXCollections.observableArrayList(publicEvent.getAttendants(this.userService));
        ComboBox<User> attendantsINFO = new ComboBox<>(attendantsList);
        attendantsINFO.setPrefSize(200, 30);

        Label timeINFO = new Label(publicEvent.getTimeToString());

        GridPane infoPane = new GridPane();

        infoPane.addRow(0, nameLBL, nameINFO);
        infoPane.addRow(1, hostLBL, hostINFO);
        infoPane.addRow(2, timeLBL, timeINFO);
        infoPane.addRow(3, placeLBL, placeINFO);
        infoPane.addRow(4, attendantsLBL, attendantsINFO);

        infoPane.setHgap(15);
        infoPane.setVgap(15);
        infoPane.setAlignment(Pos.CENTER);

        Stage stage = new Stage();
        stage.setTitle("Event Panel");
        stage.setResizable(false);
        stage.initModality(Modality.APPLICATION_MODAL);

        Button cancelButton = new Button("Exit");
        cancelButton.setOnAction(e -> stage.close());
        ToggleButton attenderButton = publicEvent.getAttendants().contains(logged.getId()) ? new ToggleButton("Stop Following") : new ToggleButton("Follow Event");
        attenderButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String text = attenderButton.getText();
                if (!text.equals("Stop Following")) {
                    attenderButton.setText("Stop Following");
                    eventService.attendToEvent(publicEvent.getId(), logged.getId());
                    attendantsList.add(logged);
                } else {
                    attenderButton.setText("Follow Event");
                    eventService.missEvent(publicEvent.getId(), logged.getId());
                    attendantsList.remove(logged);
                }
//                editEventsCells();
                events.clear();
                events.setAll(eventService.getExternEvents(logged.getId()));
                PaginatorUtils.getListPager(eventsPaginator, eventsList, events);
                updateEventsTabText();
            }
        });
        cancelButton.setPrefSize(150, 30);
        attenderButton.setPrefSize(150, 30);

        HBox buttonBox = new HBox(30, attenderButton, cancelButton);
        buttonBox.setAlignment(Pos.CENTER);

        VBox mainBox = new VBox(20, infoPane, buttonBox);
        mainBox.setAlignment(Pos.CENTER);

        Scene scene = new Scene(mainBox, 500, 300);
        scene.getStylesheets().add(styling1Res);
        stage.setScene(scene);

        stage.showAndWait();
    }

    private void myEventHandler(PublicEvent publicEvent) {

        Label attendantsLBL = new Label("Attendants");
        Label placeLBL = new Label("Place");
        Label nameLBL = new Label("Event");
        Label timeLBL = new Label("Date");

        Label placeINFO = new Label(publicEvent.getPlace());
        Label nameINFO = new Label(publicEvent.getName());
        ComboBox<User> attendantsINFO = new ComboBox<>(FXCollections.observableArrayList(publicEvent.getAttendants(this.userService)));
        attendantsINFO.setPrefSize(200, 30);

        Label timeINFO = new Label(publicEvent.getTimeToString());

        GridPane infoPane = new GridPane();

        infoPane.addRow(0, nameLBL, nameINFO);
        infoPane.addRow(1, timeLBL, timeINFO);
        infoPane.addRow(2, placeLBL, placeINFO);
        infoPane.addRow(3, attendantsLBL, attendantsINFO);

        infoPane.setHgap(15);
        infoPane.setVgap(15);
        infoPane.setAlignment(Pos.CENTER);

        Stage stage = new Stage();
        stage.setTitle("My Event Panel");
        stage.setResizable(false);
        stage.initModality(Modality.APPLICATION_MODAL);

        Button cancelButton = new Button("Exit");
        cancelButton.setOnAction(event -> stage.close());
        Button removeButton = new Button("Remove Event");
        removeButton.setOnAction(event -> {
            eventService.clearEvent(publicEvent.getId());
            events.remove(publicEvent);
            PaginatorUtils.getListPager(eventsPaginator, eventsList, events);
            MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Success", "This event was completely deleted!");
            updateEventsTabText();
            editEventsCells();
            stage.close();
        });
        cancelButton.setPrefSize(150, 30);
        removeButton.setPrefSize(150, 30);

        HBox buttonBox = new HBox(30, removeButton, cancelButton);
        buttonBox.setAlignment(Pos.CENTER);

        VBox mainBox = new VBox(20, infoPane, buttonBox);
        mainBox.setAlignment(Pos.CENTER);

        Scene scene = new Scene(mainBox, 500, 250);
        scene.getStylesheets().add(styling1Res);
        stage.setScene(scene);

        stage.showAndWait();

    }

    @FXML
    Label friendsNumberLabel;
    @FXML
    TableView<ExternUser> friendsTable;
    @FXML
    TableColumn<ExternUser, String> friendsNameColumn;
    @FXML
    TableColumn<ExternUser, LocalDate> friendsDateColumn;
    @FXML
    ListView nonFriendsListView;


    private void updateFriendsNoLabel() {
        Integer friendsNO = this.logged.getFriends().size();
        if (friendsNO == 1)
            friendsNumberLabel.setText("You have " + this.logged.getFriends().size() + " friend!");
        else friendsNumberLabel.setText("You have " + this.logged.getFriends().size() + " friends!");
    }

    @FXML
    Pagination friendsPaginator;
    @FXML
    Pagination nonFriendsPaginator;


    private void updateFriendsTab() {


        friendships.setAll(createFriendsList());

        friendsNameColumn.setCellValueFactory(new PropertyValueFactory<ExternUser, String>("name"));
        friendsDateColumn.setCellValueFactory(new PropertyValueFactory<ExternUser, LocalDate>("localDate"));

        PaginatorUtils.getTablePager(friendsPaginator, friendsTable, friendships);


        nonFriends.setAll(this.friendshipService.findUsersThatAreNotMyFriends(this.logged));
        PaginatorUtils.getListPager(nonFriendsPaginator, nonFriendsListView, nonFriends);
        //        nonFriendsListView.setItems(nonFriends);
    }

    @FXML
    TableView<FriendRequest> sentTable;
    @FXML
    TableView<FriendRequest> receivedTable;

    @FXML
    TableColumn<FriendRequest, String> toSentColumn;
    @FXML
    TableColumn<FriendRequest, LocalDate> atSentColumn;
    @FXML
    TableColumn<FriendRequest, String> statusSentColumn;

    @FXML
    TableColumn<FriendRequest, String> fromReceivedColumn;
    @FXML
    TableColumn<FriendRequest, LocalDate> atReceivedColumn;
    @FXML
    TableColumn<FriendRequest, String> statusReceivedColumn;

    @FXML
    Label sentLabel;
    @FXML
    Label receivedLabel;

    @FXML
    ToggleButton toggleRequests;
    @FXML
    StackPane requestsPane;
    @FXML
    VBox sentBox;
    @FXML
    VBox receivedBox;

    public void receivedRequestsHandler(ActionEvent actionEvent) {
        final DoubleProperty opacity = requestsPane.opacityProperty();
        Timeline fade = null;
        if (toggleRequests.isSelected()) {
            fade = new Timeline(
                    new KeyFrame(Duration.ZERO, new KeyValue(opacity, 1.0)),
                    new KeyFrame(new Duration(1000), new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            requestsPane.getChildren().remove(sentBox);
                            requestsPane.getChildren().add(receivedBox);
                            Timeline fadeIn = new Timeline(
                                    new KeyFrame(Duration.ZERO, new KeyValue(opacity, 0.0)),
                                    new KeyFrame(new Duration(800), new KeyValue(opacity, 1.0))
                            );
                            fadeIn.play();
                        }
                    }, new KeyValue(opacity, 0.0)));
            toggleRequests.setText("View Sent Requests");
        } else {
            fade = new Timeline(
                    new KeyFrame(Duration.ZERO, new KeyValue(opacity, 1.0)),
                    new KeyFrame(new Duration(1000), new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            requestsPane.getChildren().remove(receivedBox);
                            requestsPane.getChildren().add(sentBox);
                            Timeline fadeIn = new Timeline(
                                    new KeyFrame(Duration.ZERO, new KeyValue(opacity, 0.0)),
                                    new KeyFrame(new Duration(800), new KeyValue(opacity, 1.0))
                            );
                            fadeIn.play();
                        }
                    }, new KeyValue(opacity, 0.0)));
            toggleRequests.setText("View Received Requests");
        }
        fade.play();
    }

    @FXML
    Pagination sentRequestsPaginator;
    @FXML
    Pagination receivedRequestsPaginator;

    private void updateRequestsTab() {
        Predicate<Friendship> isSent = f -> f.getId().getLeft() == this.logged.getId();
        Predicate<Friendship> isReceived = f -> f.getId().getRight() == this.logged.getId();

        sentRequests.setAll(this.friendshipService.getAllRequests(logged, isSent));
        receivedRequests.setAll(this.friendshipService.getAllRequests(logged, isReceived));

        toSentColumn.setCellValueFactory(new PropertyValueFactory<FriendRequest, String>("name"));
        fromReceivedColumn.setCellValueFactory(new PropertyValueFactory<FriendRequest, String>("name"));
        atSentColumn.setCellValueFactory(new PropertyValueFactory<FriendRequest, LocalDate>("localDate"));
        atReceivedColumn.setCellValueFactory(new PropertyValueFactory<FriendRequest, LocalDate>("localDate"));
        statusSentColumn.setCellValueFactory(new PropertyValueFactory<FriendRequest, String>("status"));
        statusReceivedColumn.setCellValueFactory(new PropertyValueFactory<FriendRequest, String>("status"));

        PaginatorUtils.getTablePager(sentRequestsPaginator, sentTable, sentRequests);
        PaginatorUtils.getTablePager(receivedRequestsPaginator, receivedTable, receivedRequests);

        sentLabel.setText("Sent requests: " + sentRequests.size() + " available!");
        receivedLabel.setText("Received requests: " + receivedRequests.size() + " available!");

        toggleRequests.setSelected(false);
        requestsPane.getChildren().remove(receivedBox);
        toggleRequests.setText("View Received Requests");
    }


    ObservableList<Correspondent> correspondents = FXCollections.observableArrayList();
    @FXML
    TableView<Correspondent> correspondentTable;
    @FXML
    TableColumn<Correspondent, String> nameCorrespondentColumn;
    @FXML
    TableColumn<Correspondent, CheckBox> selectCorrespondentColumn;

    private List<Correspondent> getCorrespondents() {
        return this.logged.getFriends().stream().map(this.userService::findUser).map(Correspondent::new).collect(Collectors.toList());
    }

    @FXML
    Pagination correspondentsPaginator;

    private void updateMessageTab() {
        correspondents.setAll(getCorrespondents());

        nameCorrespondentColumn.setCellValueFactory(new PropertyValueFactory<Correspondent, String>("name"));
        selectCorrespondentColumn.setCellValueFactory(new PropertyValueFactory<Correspondent, CheckBox>("selector"));

        correspondentTable.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) {
                // Get the table header
                Pane header = (Pane) correspondentTable.lookup("TableHeaderRow");
                if (header != null && header.isVisible()) {
                    header.setMaxHeight(0);
                    header.setMinHeight(0);
                    header.setPrefHeight(0);
                    header.setVisible(false);
                    header.setManaged(false);
                }
            }
        });

        PaginatorUtils.getTablePager(correspondentsPaginator, correspondentTable, correspondents);

        blockedUsers = this.friendshipService.getBlockedUsers(logged, f -> f.getId().getLeft() == logged.getId())
                .stream().map(elem -> elem.getLeft().getRight()).collect(Collectors.toList());
    }

    private List<ExternUser> createFriendsList() {
        updateFriendsNoLabel();
        return friendshipService.getMyFriends(this.logged)
                .stream().map(tuple -> new ExternUser(tuple.getLeft(), tuple.getRight())).collect(Collectors.toList());
    }

    public void friendSelected(MouseEvent mouseEvent) {
        try {
            ExternUser friend = this.friendsTable.getSelectionModel().getSelectedItem();

            List<Object> objectList = new ArrayList<>();
            objectList.add(userService);
            objectList.add(friendshipService);
            objectList.add(messageService);
            objectList.add(eventService);
            objectList.add(logged);
            objectList.add(friend);

            myController.loadScreen(screen3ID, screen3Res, objectList);

            myController.setScreen(screen3ID);

            this.friendsTable.getSelectionModel().clearSelection();

        } catch (NullPointerException n) {
        }
    }

    @Override
    public void setScreenParent(ScreensController screenPage) {
        myController = screenPage;
        String title = "Welcome " + this.logged.getName() + "!";
        Stage myStage = (Stage) myController.getScene().getWindow();
        myStage.setTitle(title);
    }

    @Override
    public void setEnvironment(List<Object> args) {
        this.userService = (UserService) args.get(0);
        this.friendshipService = (FriendshipService) args.get(1);
        this.messageService = (MessageService) args.get(2);
        this.eventService = (EventService) args.get(3);
        this.logged = (User) args.get(4);
        initializeInterface();
        initialize_filters();
    }

    public void initialize_filters() {
        usersNameFilter.textProperty().addListener(((observable, old, newValue) ->
                PaginatorUtils.getListPager(nonFriendsPaginator, nonFriendsListView,
                        nonFriends.filtered(data -> data.getFirstName().startsWith(newValue) || data.getLastName().startsWith(newValue)))
        ));

        usersNameFilter.textProperty().addListener(((observable, oldV, newV) ->
                PaginatorUtils.getTablePager(friendsPaginator, friendsTable,
                        friendships.filtered(user -> user.getUser().getFirstName().startsWith(newV) || user.getUser().getLastName().startsWith(newV)))

        ));
    }

    public void addSelectedUser(ActionEvent actionEvent) {
        try {
            User selected = (User) nonFriendsListView.getSelectionModel().getSelectedItem();
            usersNameFilter.clear();
            this.friendshipService.addFriend(logged, selected);
            MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Success", "A friend request to " + selected.getName() + " has been sent!");
            nonFriends.setAll(this.friendshipService.findUsersThatAreNotMyFriends(this.logged));
            Predicate<Friendship> isSent = f -> f.getId().getLeft() == this.logged.getId();
            sentRequests.setAll(this.friendshipService.getAllRequests(logged, isSent));
            sentLabel.setText("Sent requests: " + sentRequests.size() + " available!");
            nonFriendsListView.getSelectionModel().clearSelection();

            PaginatorUtils.getListPager(nonFriendsPaginator, nonFriendsListView, nonFriends);
            PaginatorUtils.getTablePager(sentRequestsPaginator, sentTable, sentRequests);

        } catch (NullPointerException n) {
            MessageAlert.showMessage(null, Alert.AlertType.WARNING, "Fail", "You need to select an user before clicking the button!");
        }
    }

    public void cancelRequestHandler(MouseEvent mouseEvent) {
        try {
            FriendRequest friendRequest = sentTable.getSelectionModel().getSelectedItem();
            if (!friendRequest.getStatus().equals("pending"))
                MessageAlert.showMessage(null, Alert.AlertType.WARNING, "Not a valid choice!", "You can only cancel pending requests.");
            else {
                Stage stage = new Stage();
                stage.initModality(Modality.APPLICATION_MODAL);

                stage.setTitle("Retrieve request");

                Label infoLabel = new Label("Do you wish to cancel this\n friend request?");

                infoLabel.setTextAlignment(TextAlignment.CENTER);
                infoLabel.setWrapText(true);

                Button yesButton = new Button("Yes");
                Button noButton = new Button("No");

                yesButton.setPrefSize(50, 20);
                noButton.setPrefSize(50, 20);


                VBox vBox = new VBox(10);
                HBox hBox = new HBox(10);

                yesButton.setOnAction(e -> {
                    usersNameFilter.clear();
                    friendshipService.removeFriend(logged, friendRequest.getUser());

                    Predicate<Friendship> isSent = f -> f.getId().getLeft() == logged.getId();
                    sentRequests.setAll(friendshipService.getAllRequests(logged, isSent));
                    sentLabel.setText("Sent requests: " + sentRequests.size() + " available!");
                    nonFriends.setAll(friendshipService.findUsersThatAreNotMyFriends(logged));

                    PaginatorUtils.getTablePager(sentRequestsPaginator, sentTable, sentRequests);
                    PaginatorUtils.getListPager(nonFriendsPaginator, nonFriendsListView, nonFriends);

                    MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Success", "The request has been cancelled!");
                    stage.close();
                });
                noButton.setOnAction(event -> stage.close());

                hBox.getChildren().addAll(yesButton, noButton);

                hBox.setPadding(new Insets(20, 10, 20, 10));

                vBox.getChildren().addAll(infoLabel, hBox);

                vBox.setAlignment(Pos.CENTER);
                hBox.setAlignment(Pos.CENTER);

                Scene sc = new Scene(vBox, 250, 150);

                stage.setScene(sc);
                sc.getStylesheets().add(styling1Res);

                stage.showAndWait();
            }
            sentTable.getSelectionModel().clearSelection();
        } catch (NullPointerException n) {
        }
    }

    public void respondRequestHandler(MouseEvent mouseEvent) {
        try {
            FriendRequest friendRequest = receivedTable.getSelectionModel().getSelectedItem();
            usersNameFilter.clear();
            if (friendRequest.getStatus().equals("approved"))
                MessageAlert.showMessage(null, Alert.AlertType.WARNING, "Not a valid choice!", "An approved request cannot be answered anymore.");
            else if (friendRequest.getStatus().equals("rejected"))
                answerRejectedRequest(friendRequest);
            else answerPendingRequest(friendRequest);
            receivedTable.getSelectionModel().clearSelection();
        } catch (NullPointerException ignored) {
        }
    }

    private void answerRejectedRequest(FriendRequest friendRequest) {
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setOpacity(1);
        stage.setResizable(false);


        stage.setTitle("Unblock user menu");

        Label infoLabel = new Label("Do you wish to unblock " + friendRequest.getName() + "?");

        infoLabel.setTextAlignment(TextAlignment.CENTER);
        infoLabel.setWrapText(true);

        Button yesButton = new Button("Yes");
        Button noButton = new Button("No");

        yesButton.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        noButton.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

        VBox mainBox = new VBox(5);
        HBox buttonBox = new HBox(10);

        EventHandler<ActionEvent> clickProceedEvent = e -> {

            friendshipService.unblockUser(logged, friendRequest.getUser().getId());
            MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Operation completed", friendRequest.getName() + " was unblocked!");

            nonFriends.setAll(friendshipService.findUsersThatAreNotMyFriends(logged));

            Predicate<Friendship> isReceived = f -> f.getId().getRight() == logged.getId();
            receivedRequests.setAll(friendshipService.getAllRequests(logged, isReceived));

            PaginatorUtils.getListPager(nonFriendsPaginator, nonFriendsListView, nonFriends);
            PaginatorUtils.getTablePager(receivedRequestsPaginator, receivedTable, receivedRequests);

            receivedLabel.setText("Received requests: " + receivedRequests.size() + " available!");
            stage.close();
        };

        yesButton.setOnAction(clickProceedEvent);
        noButton.setOnAction(event -> stage.close());

        buttonBox.getChildren().addAll(yesButton, noButton);
        buttonBox.setPadding(new Insets(20, 10, 20, 10));

        mainBox.getChildren().addAll(infoLabel, buttonBox);

        mainBox.setAlignment(Pos.CENTER);
        buttonBox.setAlignment(Pos.CENTER);


        Scene sc = new Scene(mainBox, 200, 150);
        stage.setScene(sc);
        sc.getStylesheets().add(styling1Res);

        stage.showAndWait();
    }

    private void answerPendingRequest(FriendRequest friendRequest) {
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setOpacity(1);
        stage.setResizable(false);


        stage.setTitle("Request from " + friendRequest.getName());

        Label infoLabel = new Label("What's your response regarding\n this friend request?");

        infoLabel.setTextAlignment(TextAlignment.CENTER);
        infoLabel.setWrapText(true);

        Button acceptButton = new Button("Accept");
        Button rejectButton = new Button("Reject");
        Button blockButton = new Button("Block");
        Button cancelButton = new Button("Cancel");

        acceptButton.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        rejectButton.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        blockButton.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        cancelButton.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

        VBox mainBox = new VBox(5);
        GridPane buttonPane = new GridPane();

        EventHandler<ActionEvent> acceptRequestEvent = e -> {
            friendshipService.processRequest(logged, friendRequest.getUser().getId(), "accept");
            MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Operation completed", friendRequest.getName() + " was accepted as friend!");

            Predicate<Friendship> isReceived = f -> f.getId().getRight() == logged.getId();
            receivedRequests.setAll(friendshipService.getAllRequests(logged, isReceived));

            friendships.setAll(createFriendsList());
            correspondents.setAll(getCorrespondents());

            PaginatorUtils.getTablePager(receivedRequestsPaginator, receivedTable, receivedRequests);
            PaginatorUtils.getTablePager(friendsPaginator, friendsTable, friendships);
            PaginatorUtils.getTablePager(correspondentsPaginator, correspondentTable, correspondents);

            stage.close();
        };


        EventHandler<ActionEvent> rejectRequestEvent = e -> {
            friendshipService.processRequest(logged, friendRequest.getUser().getId(), "reject");
            MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Operation completed", friendRequest.getName() + " was rejected!");

            nonFriends.setAll(friendshipService.findUsersThatAreNotMyFriends(logged));

            Predicate<Friendship> isReceived = f -> f.getId().getRight() == logged.getId();
            receivedRequests.setAll(friendshipService.getAllRequests(logged, isReceived));

            PaginatorUtils.getListPager(nonFriendsPaginator, nonFriendsListView, nonFriends);
            PaginatorUtils.getTablePager(receivedRequestsPaginator, receivedTable, receivedRequests);

            receivedLabel.setText("Received requests: " + receivedRequests.size() + " available!");
            stage.close();
        };

        EventHandler<ActionEvent> blockRequestEvent = e -> {
            friendshipService.processRequest(logged, friendRequest.getUser().getId(), "block");
            MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Operation completed", friendRequest.getName() + " was blocked!");

            Predicate<Friendship> isReceived = f -> f.getId().getRight() == logged.getId();
            receivedRequests.setAll(friendshipService.getAllRequests(logged, isReceived));

            PaginatorUtils.getTablePager(receivedRequestsPaginator, receivedTable, receivedRequests);

            stage.close();
        };

        acceptButton.setOnAction(acceptRequestEvent);
        rejectButton.setOnAction(rejectRequestEvent);
        blockButton.setOnAction(blockRequestEvent);
        cancelButton.setOnAction(event -> stage.close());

        buttonPane.addRow(0, acceptButton, rejectButton);
        buttonPane.addRow(1, blockButton, cancelButton);

        mainBox.getChildren().addAll(infoLabel, buttonPane);

        buttonPane.setHgap(10.0);
        buttonPane.setVgap(10.0);
        buttonPane.setPadding(new Insets(20, 10, 20, 10));

        mainBox.setAlignment(Pos.CENTER);
        buttonPane.setAlignment(Pos.CENTER);

        Scene sc = new Scene(mainBox, 280, 180);
        stage.setScene(sc);
        sc.getStylesheets().add(styling1Res);
        stage.showAndWait();
    }

    @FXML
    Button composeMsgButton;
    @FXML
    TextArea composeMsgText;

    public void sendMessageHandler(ActionEvent actionEvent) {
        Predicate<Correspondent> getSelectedUser = c -> c.getSelector().isSelected();
        List<User> selectedCorrespondents = correspondents.stream().filter(getSelectedUser).map(Correspondent::getUser).collect(Collectors.toList());
        if (selectedCorrespondents.size() == 0)
            MessageAlert.showMessage(null, Alert.AlertType.WARNING, "Message not sent", "You need to select users to sent your message to!");
        else if (composeMsgText.getText().strip().length() == 0)
            MessageAlert.showMessage(null, Alert.AlertType.WARNING, "Message not sent", "You need to write a message in the destinated field!");
        else {
            Long availableID = this.messageService.getAvailableID();
            String messageLine = availableID + ";" + selectedCorrespondents.stream()
                    .map(User::getId).map(Objects::toString).reduce("", (x, y) -> x.concat(",").concat(y)).substring(1)
                    + ";" + composeMsgText.getText().strip();
            this.messageService.sendMessage(this.logged, messageLine, false, blockedUsers);
            MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Success", "Message sent with ID=" + availableID + "!");
        }
        correspondents.forEach(c -> c.setSelector(false));
        composeMsgText.clear();
    }

    public void handleLogOutButton(ActionEvent actionEvent) {
        List<Object> objectList = new ArrayList<>();
        objectList.add(this.userService);
        objectList.add(this.friendshipService);
        objectList.add(this.messageService);
        objectList.add(this.eventService);

        myController.unloadScreen(screen1ID);
        myController.loadScreen(screen1ID, screen1Res, objectList);

        myController.setScreen(screen1ID);

        String title = "Social Network";
        Stage myStage = (Stage) myController.getScene().getWindow();
        myStage.setTitle(title);

        myController.unloadScreen(screen2ID);
    }

    public void handleDeleteAccountButton(ActionEvent actionEvent) {
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);


        stage.setTitle("Warning");

        Label infoLabel = new Label("You will lose all friends and messages. Proceed?");

        infoLabel.setTextAlignment(TextAlignment.CENTER);
        infoLabel.setWrapText(true);

        Button yesButton = new Button("Yes");
        Button noButton = new Button("No");

        yesButton.setPrefSize(50, 20);
        noButton.setPrefSize(50, 20);

        VBox mainBox = new VBox(10);
        HBox buttonBox = new HBox(10);

        EventHandler<ActionEvent> clickProceedEvent = e -> {

            myController.unloadScreen(screen1ID);
            friendshipService.updateLists(logged);
            messageService.updateMessages(logged);
            eventService.updateEvents(logged.getId());

            userService.deleteUser(logged.getId());
            myController.loadScreen(screen1ID, screen1Res, Arrays.asList(userService, friendshipService, messageService, eventService));

            MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Success", "Your account has been deleted! Hope we'll see you again, " + logged.getLastName() + "!");

            myController.setScreen(screen1ID);

            String title = "Social Network";
            Stage myStage = (Stage) myController.getScene().getWindow();
            myStage.setTitle(title);

            myController.unloadScreen(screen2ID);
            stage.close();
        };

        yesButton.setOnAction(clickProceedEvent);
        noButton.setOnAction(event -> stage.close());

        buttonBox.getChildren().addAll(yesButton, noButton);

        buttonBox.setPadding(new Insets(20, 10, 20, 10));

        mainBox.getChildren().addAll(infoLabel, buttonBox);

        mainBox.setAlignment(Pos.CENTER);
        buttonBox.setAlignment(Pos.CENTER);

        Scene sc = new Scene(mainBox, 250, 150);

        stage.setScene(sc);
        sc.getStylesheets().add(styling1Res);

        stage.showAndWait();
    }

    @FXML
    DatePicker sinceDatePicker;
    @FXML
    DatePicker untilDatePicker;

    private ObservableList<Activity> getActivitiesList() {
        ObservableList<Activity> list = FXCollections.observableArrayList();
        list.addAll(friendships.stream().map(Activity::new).collect(Collectors.toList()));
        list.addAll(this.messageService.getMyMessages(this.logged).stream().map(Activity::new).collect(Collectors.toList()));

        list.sort((o1, o2) -> {
            if (o1.getLocalDate().isBefore(o2.getLocalDate()))
                return 1;
            else if (o1.getLocalDate().isEqual(o2.getLocalDate()))
                return 0;
            return -1;
        });
        return list;
    }

    public void handleActivitiesShow(ActionEvent actionEvent) {
        LocalDate date1 = sinceDatePicker.getValue();
        LocalDate date2 = untilDatePicker.getValue();
        FilteredList<Activity> filteredActivities = new FilteredList<>(getActivitiesList(), a -> true);
        if (date2 == null)
            date2 = LocalDate.MAX;
        if (date1 == null)
            date1 = LocalDate.MIN;
        if (date1.isAfter(date2))
            MessageAlert.showErrorMessage(null, "Selected dates do not represent a valid period of time!");
        else if (filteredActivities.size() == 0)
            MessageAlert.showErrorMessage(null, "No activities to see!");
        else {
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setOpacity(1);
            stage.setResizable(false);
            stage.setTitle("Activities Preview");

            final LocalDate sinceDate = date1;
            final LocalDate untilDate = date2;

            int month1 = sinceDate.getMonthValue() - 1;
            int month2 = untilDate.getMonthValue() - 1;
            if (month1 > month2) {
                month1 += month2;
                month2 = month1 - month2;
                month1 -= month2;
            }

            filteredActivities.setPredicate(a -> (a.getLocalDate().isAfter(sinceDate) || a.getLocalDate().isEqual(sinceDate)) && (a.getLocalDate().isBefore(untilDate) || a.getLocalDate().isEqual(untilDate)));

            ListView<Activity> activityListView = new ListView<>();

            Pagination activityPaginator = new Pagination();
            PaginatorUtils.getListPager(activityPaginator, activityListView, filteredActivities);

            activityPaginator.setMaxSize(400, 230);
            activityPaginator.setMinSize(400, 230);

            TextField filenameField = new TextField();
            filenameField.setMaxSize(315, 30);
            filenameField.setMinSize(315, 30);

            filenameField.setPromptText("insert your pdf filename here ...");


            Button savePDFButton = new Button("Save as PDF");
            Button exitButton = new Button("Exit");

            savePDFButton.setPrefSize(150, 20);
            exitButton.setPrefSize(150, 20);

            VBox mainBox = new VBox(15);
            HBox infoBox = new HBox(15);
            HBox buttonBox = new HBox(15);

            CategoryAxis xAxis = new CategoryAxis();
            xAxis.setLabel("Month");
            NumberAxis yAxis = new NumberAxis();
            yAxis.setLabel("Activities NO.");

            LineChart<String, Number> activitiesChart = new LineChart<String, Number>(xAxis, yAxis);
            activitiesChart.setTitle("Selected Activities no. by month");

            XYChart.Series activitySeries = new XYChart.Series();
            XYChart.Series messagesSeries = new XYChart.Series();
            XYChart.Series friendsSeries = new XYChart.Series();
            activitySeries.setName("Monthly No. of Activities");
            messagesSeries.setName("Monthly No. of Messages sent");
            friendsSeries.setName("Monthly No. of Friends added");

            List<String> months = Arrays.asList("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
            for (int i = month1; i <= month2; i++) {
                Month month = Month.of(i + 1);
                Number countAll = filteredActivities.stream().filter(a -> a.getLocalDate().getMonth() == month).count();
                Number countMessages = filteredActivities.stream().filter(a -> a.getLocalDate().getMonth() == month).filter(Activity::isMessage).count();
                Number countFriends = filteredActivities.stream().filter(a -> a.getLocalDate().getMonth() == month).filter(Activity::isFriend).count();
                activitySeries.getData().add(new XYChart.Data(months.get(i), countAll));
                messagesSeries.getData().add(new XYChart.Data(months.get(i), countMessages));
                friendsSeries.getData().add(new XYChart.Data(months.get(i), countFriends));
            }
            activitiesChart.getData().addAll(friendsSeries,messagesSeries,activitySeries);

            activitiesChart.setPrefWidth(375);

            EventHandler<ActionEvent> clickProceedEvent = e -> {
                try {
                    String filename = filenameField.getText().strip();
                    if (filename.length() == 0)
                        MessageAlert.showErrorMessage(null, "You did not specify the filename for the PDF!");
                    else if (!filename.endsWith(".pdf") || filename.length() == 4 || filename.contains("/") || filename.contains(("\\")))
                        MessageAlert.showErrorMessage(null, "Use file_name.pdf format to save the file!");
                    else {
                        Document document = new Document();
                        PdfWriter.getInstance(document, new FileOutputStream(REPORTS_PATH + filename));

                        document.open();

                        com.itextpdf.text.Font font = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 10, BaseColor.RED);

                        Paragraph title = new Paragraph(logged.getName() + "'s activities from the selected period\n" + setPeriod(sinceDate, untilDate), FontFactory.getFont(FontFactory.HELVETICA_BOLD, 15, BaseColor.BLACK));
                        title.setAlignment(Rectangle.ALIGN_CENTER);


                        PdfPTable friendsPdfTable = new PdfPTable(2);
                        Stream.of(new Chunk("Date", font), new Chunk("Name", font)).forEach(
                                columnTitle -> {
                                    PdfPHeaderCell header = new PdfPHeaderCell();
                                    header.setBackgroundColor(BaseColor.YELLOW);
                                    header.setBorderWidth(1);

                                    header.setHorizontalAlignment(Element.ALIGN_CENTER);
                                    header.setVerticalAlignment(Element.ALIGN_CENTER);

                                    Phrase phrase = new Phrase(columnTitle);
                                    Paragraph paragraph = new Paragraph(phrase);
                                    paragraph.setAlignment(Rectangle.ALIGN_CENTER);
                                    header.setPhrase(phrase);

                                    friendsPdfTable.addCell(header);
                                }
                        );
                        filteredActivities.stream().filter(Activity::isFriend).forEach(a -> addFriendToTable(friendsPdfTable, a.getExternUser()));

                        PdfPTable messagePdfTable = new PdfPTable(3);
                        Stream.of(new Chunk("Date", font), new Chunk("To", font),
                                new Chunk("Message text", font))
                                .forEach(columnTitle -> {
                                    PdfPHeaderCell header = new PdfPHeaderCell();
                                    header.setBackgroundColor(BaseColor.YELLOW);
                                    header.setBorderWidth(1);

                                    header.setHorizontalAlignment(Element.ALIGN_CENTER);
                                    header.setVerticalAlignment(Element.ALIGN_CENTER);

                                    Phrase phrase = new Phrase(columnTitle);
                                    Paragraph paragraph = new Paragraph(phrase);
                                    paragraph.setAlignment(Rectangle.ALIGN_CENTER);
                                    header.setPhrase(phrase);


                                    messagePdfTable.addCell(header);
                                });
                        filteredActivities.stream().filter(Activity::isMessage).forEach(a -> addMessageToTable(messagePdfTable, a.getText()));

                        document.add(title);
                        document.add(new Phrase("\n\na.Friends added\n\n"));
                        document.add(friendsPdfTable);
                        document.add(new Phrase("\n\nb.Messages sent\n\n"));
                        document.add(messagePdfTable);

                        document.close();

                        MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Success", filename + " was generated!");
                        stage.close();

                        if (Desktop.isDesktopSupported()) {
                            Desktop desktop = Desktop.getDesktop();
                            File file = new File(REPORTS_PATH + filename);
                            desktop.open(file);
                        }
                    }
                } catch (FileNotFoundException | DocumentException f) {
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            };


            savePDFButton.setOnAction(clickProceedEvent);
            exitButton.setOnAction(e -> stage.close());

            buttonBox.getChildren().addAll(savePDFButton, exitButton);
            Insets insets = new Insets(10, 20, 10, 20);
            buttonBox.setPadding(insets);
            infoBox.setPadding(insets);

            infoBox.getChildren().addAll(activityPaginator, activitiesChart);
            mainBox.getChildren().addAll(infoBox, filenameField, buttonBox);

            infoBox.setAlignment(Pos.CENTER);
            mainBox.setAlignment(Pos.CENTER);
            buttonBox.setAlignment(Pos.CENTER);

            Scene sc = new Scene(mainBox, 800, 500);
            stage.setScene(sc);
            sc.getStylesheets().add(styling1Res);
            stage.showAndWait();
        }
        sinceDatePicker.setValue(null);
        untilDatePicker.setValue(null);
    }

    private String setPeriod(LocalDate sinceDate, LocalDate untilDate) {
        if (sinceDate == LocalDate.MIN && untilDate == LocalDate.MAX)
            return "(all time)";
        else if (sinceDate == LocalDate.MIN)
            return "(until " + untilDate.format(DATE_FORMATTER) + ")";
        else if (untilDate == LocalDate.MAX)
            return "(since " + sinceDate.format(DATE_FORMATTER) + ")";
        return "(" + sinceDate.format(DATE_FORMATTER) + "  -  " + untilDate.format(DATE_FORMATTER) + ")";
    }

    private void addMessageToTable(PdfPTable table, Message message) {
        List<PdfPCell> to = message.getTo().stream().map(userService::findUser)
                .map(User::getName).map(Phrase::new).map(PdfPCell::new).collect(Collectors.toList());
        String date = message.getData().format(DATE_TIME_FORMATTER);

        PdfPCell cell1 = new PdfPCell(new Phrase(date));
        cell1.setBackgroundColor(BaseColor.YELLOW);
        cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell1.setVerticalAlignment(Element.ALIGN_CENTER);
        cell1.setBorderWidth(1);

        PdfPTable toTable = new PdfPTable(1);
        to.forEach(c -> {
            c.setBackgroundColor(BaseColor.YELLOW);
            c.setHorizontalAlignment(Element.ALIGN_CENTER);
            c.setVerticalAlignment(Element.ALIGN_CENTER);
            c.setBorderWidth(1);
        });
        to.forEach(toTable::addCell);
        PdfPCell cell2 = new PdfPCell(toTable);
        PdfPCell cell3 = new PdfPCell(new Phrase(message.getMessage()));
        cell3.setBackgroundColor(BaseColor.YELLOW);
        cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell3.setVerticalAlignment(Element.ALIGN_CENTER);
        cell3.setBorderWidth(1);

        table.addCell(cell1);
        table.addCell(cell2);
        table.addCell(cell3);
    }

    private void addFriendToTable(PdfPTable table, ExternUser friend) {
        PdfPCell cell1 = new PdfPCell(new Phrase(friend.getLocalDate().format(DATE_FORMATTER)));
        cell1.setBackgroundColor(BaseColor.YELLOW);
        cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell1.setVerticalAlignment(Element.ALIGN_CENTER);
        cell1.setBorderWidth(1);

        PdfPCell cell2 = new PdfPCell(new Phrase(friend.getName()));
        cell2.setBackgroundColor(BaseColor.YELLOW);
        cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell2.setVerticalAlignment(Element.ALIGN_CENTER);
        cell2.setBorderWidth(1);

        table.addCell(cell1);
        table.addCell(cell2);
    }

    public void handleChangePass(ActionEvent actionEvent) {
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setOpacity(1);
        stage.setResizable(false);
        stage.setTitle("Change Password Panel");

        PasswordField oldPassField = new PasswordField();
        PasswordField newPassField = new PasswordField();
        PasswordField repNewPassField = new PasswordField();


        Label lbl1 = new Label("Current Password");
        Label lbl2 = new Label("New Password");
        Label lbl3 = new Label("Confirm Password");

        Button submitButton = new Button("Submit");
        Button cancelButton = new Button("Cancel");

        submitButton.setPrefSize(100, 40);
        cancelButton.setPrefSize(100, 40);

        submitButton.setOnAction(event -> {
            String oldPass = oldPassField.getText().strip();
            String newPass = newPassField.getText().strip();
            String repPass = repNewPassField.getText().strip();
            try {
                if (!PasswordUtils.verifyUserPassword(oldPass, logged.getPassword(), logged.getSalt()))
                    throw new GuiException("You didn't gave us the correct password!");
                if (newPass.length() < 8 || !newPass.matches("^[0-9a-zA-Z]+$"))
                    throw new GuiException("Password must have at least 8 characters or numbers!");
                if (!newPass.equals(repPass))
                    throw new GuiException("Given new passwords don't match!");
                String userline = logged.getId() + ";" + logged.getName().replace(" ", ";") + ";" + newPass;

                User updated = userService.updateUser(userline);
                if (updated != null)
                    throw new GuiException("An error occured. Password was not updated!");

                User updatedOne = userService.findUser(logged.getId());

                myController.unloadScreen(screen2ID);
                MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Succes", "Password was updated, you'll be redirected to your page!");
                stage.close();

                List<Object> objectList = new ArrayList<>();
                objectList.add(userService);
                objectList.add(friendshipService);
                objectList.add(messageService);
                objectList.add(eventService);
                objectList.add(updatedOne);
                myController.loadScreen(screen2ID, screen2Res, objectList);
                myController.setScreen(screen2ID);

            } catch (GuiException g) {
                MessageAlert.showErrorMessage(null, g.getMessage());
            } finally {
                oldPassField.clear();
                newPassField.clear();
                repNewPassField.clear();
            }
        });
        cancelButton.setOnAction(event -> stage.close());

        HBox buttonBox = new HBox();
        GridPane infoBox = new GridPane();
        VBox mainBox = new VBox();

        infoBox.addRow(0, lbl1, oldPassField);
        infoBox.addRow(1, lbl2, newPassField);
        infoBox.addRow(2, lbl3, repNewPassField);

        buttonBox.getChildren().addAll(submitButton, cancelButton);

        infoBox.setHgap(25);
        infoBox.setVgap(25);
        infoBox.setPadding(new Insets(10, 20, 10, 20));
        infoBox.setAlignment(Pos.CENTER);

        buttonBox.setSpacing(15);
        buttonBox.setAlignment(Pos.CENTER);

        mainBox.getChildren().addAll(infoBox, buttonBox);
        mainBox.setSpacing(30);
        mainBox.setAlignment(Pos.CENTER);

        Scene sc1 = new Scene(mainBox, 500, 300);
        sc1.getStylesheets().add(styling1Res);
        stage.setScene(sc1);
        stage.showAndWait();
    }

    public void handleChangeName(ActionEvent actionEvent) {
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setOpacity(1);
        stage.setResizable(false);
        stage.setTitle("Change Name Panel");

        TextField firstField = new TextField();
        TextField lastField = new TextField();

        Label stLBL = new Label("First Name");
        Label ndLBL = new Label("Last Name");

        Button submitButton = new Button("Submit");
        Button cancelButton = new Button("Cancel");

        submitButton.setPrefSize(100, 40);
        cancelButton.setPrefSize(100, 40);

        submitButton.setOnAction(event -> {
            String firstName = firstField.getText().strip();
            String lastName = lastField.getText().strip();
            try {
                String userline = logged.getId() + ";" + firstName + ";" + lastName + ";" + logged.getPassword() + ";" + logged.getSalt();
                User update = userService.updateUser(userline);
                if (update != null)
                    throw new GuiException("An error occured. Password was not updated!");

                User updatedOne = userService.findUser(logged.getId());

                myController.unloadScreen(screen2ID);
                MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Succes", "Name was updated, you'll be redirected to your page!");
                MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Info", "You will have a new login username: " + updatedOne.getUserName());
                stage.close();

                List<Object> objectList = new ArrayList<>();
                objectList.add(userService);
                objectList.add(friendshipService);
                objectList.add(messageService);
                objectList.add(eventService);
                objectList.add(updatedOne);
                myController.loadScreen(screen2ID, screen2Res, objectList);
                myController.setScreen(screen2ID);

            } catch (ValidationException | GuiException e) {
                MessageAlert.showErrorMessage(null, e.getMessage());
            }
        });
        cancelButton.setOnAction(e -> stage.close());

        GridPane infoPane = new GridPane();
        infoPane.addRow(0, stLBL, firstField);
        infoPane.addRow(1, ndLBL, lastField);

        infoPane.setAlignment(Pos.CENTER);
        infoPane.setHgap(25);
        infoPane.setVgap(25);

        HBox buttonBox = new HBox();
        buttonBox.getChildren().addAll(submitButton, cancelButton);

        buttonBox.setAlignment(Pos.CENTER);
        buttonBox.setSpacing(20);

        Label warningLBL = new Label("We recommend you to inform your friends with a message about this modification of your profile!");
        warningLBL.setTextAlignment(TextAlignment.CENTER);
        warningLBL.setWrapText(true);

        VBox mainBox = new VBox(30,warningLBL,infoPane,buttonBox);

        Scene scene = new Scene(mainBox, 500, 250);
        scene.getStylesheets().add(styling1Res);
        stage.setScene(scene);
        stage.show();
    }
}
