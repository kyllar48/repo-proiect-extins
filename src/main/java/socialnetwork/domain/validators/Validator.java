package socialnetwork.domain.validators;

/**
 * @param <T> - the entity to be validated
 */
public interface Validator<T> {
    /**
     *
     * @param entity - the object to be validated.
     * @throws ValidationException - if the entity does not respect some integrity principles.
     */
    void validate(T entity) throws ValidationException;
}