package socialnetwork.domain;

import socialnetwork.service.UserService;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static utils.Constants.DATE_TIME_FORMATTER;

public class PublicEvent extends Entity<Long>{
    private Long host;
    private List<Long> attendants;
    private String name;
    private String place;
    private LocalDateTime time;

    public PublicEvent(Long host, String name, String place, LocalDateTime time) {
        this.host = host;
        this.attendants = new ArrayList<>();
        this.name = name;
        this.place = place;
        this.time = time;
    }

    public Long getHost(){
        return host;
    }

    public User getHost(UserService userService) {
        return userService.findUser(this.host);
    }

    public List<User> getAttendants(UserService userService) {
        return this.attendants.stream().map(userService::findUser).collect(Collectors.toList());
    }

    public List<Long> getAttendants(){
        return attendants;
    }

    public void setAttendants(Long attendant, Boolean toAdd) {
        if (toAdd) this.attendants.add(attendant);
         else this.attendants.remove(attendant);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlace() {
        return place;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public String getTimeToString(){
        return time.format(DATE_TIME_FORMATTER);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PublicEvent that = (PublicEvent) o;
        return Objects.equals(host, that.host) &&
                Objects.equals(attendants, that.attendants) &&
                Objects.equals(getName(), that.getName()) &&
                Objects.equals(getPlace(), that.getPlace()) &&
                Objects.equals(getTime(), that.getTime());
    }

    @Override
    public int hashCode() {
        return Objects.hash(host, attendants, getName(), getPlace(), getTime());
    }

    @Override
    public String toString() {

        
        return this.name;
    }

    public Long getIminencyRating(Long userId){
        if(host!=userId && !attendants.contains(userId))
            return Long.MIN_VALUE;
        return Duration.between(LocalDateTime.now(),time).toDays();

    }
}
