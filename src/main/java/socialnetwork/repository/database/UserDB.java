package socialnetwork.repository.database;

import socialnetwork.domain.User;
import socialnetwork.domain.validators.Validator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class UserDB extends AbstractDBRepository<Long, User> {

    public UserDB(Validator<User> validator, String url) {
        super(validator, url);
    }


    @Override
    protected void load_data() {
        {
            try {
                String mySQL = "SELECT * FROM [First try].[User]";
                PreparedStatement statement = connection.prepareStatement(mySQL);
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    User user = new User(resultSet.getString(2), resultSet.getString(3), resultSet.getNString(4), resultSet.getNString(5));
                    user.setId(resultSet.getLong(1));
                    super.save(user);
                }

            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    @Override
    public User save(User entity) {
        User e = super.save(entity);
        if (e == null) {
            addToDB(entity);
        }
        return e;
    }


    @Override
    protected void addToDB(User entity) {
        try {

            Statement statement = connection.createStatement();
            statement.executeUpdate("SET IDENTITY_INSERT [First try].[User] ON; " +
                    "INSERT INTO [First try].[User](id_user,first_name,last_name,password,salt) " +
                    "VALUES (" + entity.getId() + "," +
                    "'" + entity.getFirstName() + "'" +
                    "," +
                    "'" + entity.getLastName() + "'" +
                    "," +
                    "N'" + entity.getPassword() + "'" +
                    "," +
                    "N'" + entity.getSalt() + "'" +
                    ")");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    protected void clearFromDB(User entity) {
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate("DELETE FROM [First try].[User] " +
                    "WHERE id_user=" + entity.getId());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public User update(User entity) {
        User e = super.update(entity);
        if (e == null)
            updateDB(entity);
        return e;
    }


    private void updateDB(User entity) {
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate("UPDATE [First try].[User]" +
                    "SET first_name=" +
                    "'" + entity.getFirstName() + "'" +
                    ", " +
                    "last_name=" +
                    "'" + entity.getLastName() + "'" +
                    ", " +
                    "password=" +
                    "N'" + entity.getPassword() + "'" +
                    ", " +
                    "salt=" +
                    "N'" + entity.getSalt() + "' "
                    + "WHERE id_user=" + entity.getId());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

}
