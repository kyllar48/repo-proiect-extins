package socialnetwork.ui.gui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.*;
import utils.PaginatorUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static utils.Constants.*;


public class MainController implements Screen {

    ObservableList<User> userObservableList = FXCollections.observableArrayList();

    private UserService userService;
    private FriendshipService friendshipService;
    private MessageService messageService;
    private EventService eventService;
    private ScreensController myController;

    @FXML
    Button addUserButton;

    public void communityInfosClick(ActionEvent actionEvent) {

        CommunityService communityService = new CommunityService(this.userService);

        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setOpacity(1);
        stage.setResizable(false);

        stage.setTitle("Honorable Mentions in the app!");

        Label infoLabel = new Label("Our application currently registers a number of " +
                + communityService.CommunityNO() + " communities!" +
                "\nThe most sociable community consists of the users shown below:");
        ObservableList<User> mentions = FXCollections.observableArrayList(communityService.MostSociableCommunity());

        ListView<User> mentionsList = new ListView<User>();

        Pagination mentionsPaginator = new Pagination();

        PaginatorUtils.getListPager(mentionsPaginator,mentionsList,mentions);

        VBox mainBox = new VBox(10);

        infoLabel.setTextAlignment(TextAlignment.CENTER);
        infoLabel.setWrapText(true);

        mentionsPaginator.setMaxSize(300,220);
        mentionsPaginator.setMinSize(300,220);
        mentionsPaginator.setPrefSize(300,220);

        mainBox.getChildren().addAll(infoLabel,mentionsPaginator);

        mainBox.setPadding(new Insets(0,10,0,10));
        mainBox.setAlignment(Pos.CENTER);

        Scene scene = new Scene(mainBox,500,350);
        scene.getStylesheets().add(styling1Res);

        stage.setScene(scene);
        stage.showAndWait();
    }

    @Override
    public void setScreenParent(ScreensController screenPage) {
        myController = screenPage;
    }

    @Override
    public void setEnvironment(List<Object> args) {
        this.userService = (UserService) args.get(0);
        this.friendshipService = (FriendshipService) args.get(1);
        this.messageService = (MessageService) args.get(2);
        this.eventService = (EventService) args.get(3);
        initializeInterface();
    }

    private void initializeInterface(){
        List<User> userList = new ArrayList<>();
        this.userService.getAll().forEach(userList::add);
        userObservableList.setAll(userList);
    }

    @FXML
    TextField usernameField;
    @FXML
    PasswordField passwordField;

    public void handleLogin(ActionEvent actionEvent) {
        String username = usernameField.getText().strip();
        String passwd = passwordField.getText();
        Optional<User> search = userObservableList.stream().filter(u -> u.getUserName().equals(username)).findFirst();
        if(search.isPresent()){
            User toLog = search.get();
            if(toLog.verifyLogin(username,passwd)){
                List<Object> objectList = new ArrayList<>();
                objectList.add(userService);
                objectList.add(friendshipService);
                objectList.add(messageService);
                objectList.add(eventService);
                objectList.add(toLog);

                myController.loadScreen(screen2ID,screen2Res,objectList);

                myController.setScreen(screen2ID);

            }else {
                MessageAlert.showErrorMessage(null,"Invalid username or password, try again!");
                passwordField.clear();
                usernameField.clear();
            }
        }else {
            MessageAlert.showErrorMessage(null,"Invalid username or password, try again!");
            passwordField.clear();
            usernameField.clear();
        }
    }

    public void handleSignup(ActionEvent actionEvent) {
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setOpacity(1);
        stage.setResizable(false);

        stage.setTitle("Sign Up Panel");

        TextField firstNameField = new TextField();
        TextField lastNameField = new TextField();
        PasswordField passwordField = new PasswordField();
        PasswordField repeaterField = new PasswordField();
        Label firstLabel = new Label("First Name");
        Label lastLabel = new Label("Last Name");
        Label passLabel = new Label("Password");
        Label repeatLabel = new Label("Re-type password");

        Button submitButton = new Button("Submit");submitButton.setPrefSize(100,20);
        Button cancelButton = new Button("Cancel");cancelButton.setPrefSize(100,20);

        cancelButton.setOnAction(e -> stage.close());

        submitButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                    String firstName = firstNameField.getText().strip();
                    String lastName = lastNameField.getText().strip();
                    String passwd = passwordField.getText().strip();
                    String repPasswd = repeaterField.getText().strip();
                    Long id = userService.getAvailableID();
                    try{
                        if(passwd.length()<8 || !passwd.matches("^[0-9a-zA-Z]+$"))
                            throw new GuiException("Password must have at least 8 characters or numbers!");
                        if (!passwd.equals(repPasswd))
                            throw new GuiException("Given passwords don't match!");
                        userService.addUser(id+";"+firstName+";"+lastName+";"+passwd);

                        User added = userService.findUser(id);
                        String username = added.getUserName();
                        MessageAlert.showMessage(null, Alert.AlertType.INFORMATION,"Success","User "
                                + username +
                                " registered!");
                        stage.close();

                        List<Object> objectList = new ArrayList<>();
                        objectList.add(userService);
                        objectList.add(friendshipService);
                        objectList.add(messageService);
                        objectList.add(eventService);
                        objectList.add(added);

                        myController.loadScreen(screen2ID,screen2Res,objectList);

                        myController.setScreen(screen2ID);


                        userObservableList.add(added);

                    }catch (GuiException | ValidationException e){
                        MessageAlert.showErrorMessage(null,e.getMessage());
                    } finally {
                        firstNameField.clear();
                        lastNameField.clear();
                        passwordField.clear();
                        repeaterField.clear();
                    }
            }
        });

        GridPane infoPane = new GridPane();
        HBox hBox = new HBox();
        VBox mainBox = new VBox();

        infoPane.addRow(0,firstLabel,firstNameField);
        infoPane.addRow(1,lastLabel,lastNameField);
        infoPane.addRow(2,passLabel,passwordField);
        infoPane.addRow(3,repeatLabel,repeaterField);

        hBox.getChildren().addAll(submitButton,cancelButton);


        infoPane.setPadding(new Insets(10,20,10,20));
        infoPane.setHgap(25);
        infoPane.setVgap(25);
        infoPane.setAlignment(Pos.CENTER);

        hBox.setAlignment(Pos.CENTER);
        hBox.setSpacing(25.0);

        mainBox.setSpacing(25.0);
        mainBox.setPadding(new Insets(10,20,10,20));

        mainBox.getChildren().addAll(infoPane,hBox);

        Scene scene = new Scene(mainBox,450,300);
        scene.getStylesheets().add(styling1Res);

        stage.setScene(scene);
        stage.showAndWait();

    }
}

