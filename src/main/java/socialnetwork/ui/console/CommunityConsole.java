package socialnetwork.ui.console;

import socialnetwork.service.CommunityService;
import socialnetwork.service.FriendshipService;
import socialnetwork.service.MessageService;
import socialnetwork.service.UserService;

import java.util.InputMismatchException;

public class CommunityConsole extends MainConsole {
    public CommunityConsole(UserService userService, FriendshipService friendshipService, MessageService messageService) {
        super(userService, friendshipService, messageService);
    }

    @Override
    public void run(){
        while (true) {
            CommunityService communityService = new CommunityService(this.userService);
            int option;
            System.out.println("1.Number of communities" + newLine
                    + "2.The most sociable community" + newLine
                    + "3.Return to main menu" + newLine);
            try {
                option = scanner.nextInt();
                switch (option) {
                    case 3: {
                        System.out.println("Closing community menu..." + newLine);
                        return;
                    }
                    case 2: {
                        System.out.println("===================Most sociable community==================");
                        communityService.MostSociableCommunity().forEach(System.out::println);
                        System.out.println("=================================================");
                        break;
                    }
                    case 1: {
                        System.out.println("There are "
                                + communityService.CommunityNO()
                                + " communities in the network!"
                                + newLine);
                        break;
                    }
                    default: {
                        System.out.println("No valid option!" + newLine);
                        break;
                    }
                }
            } catch (InputMismatchException inputMismatchException) {
                System.err.println("You need to give a number to proceed" + newLine);
                scanner.nextLine();
            }

        }
    }
}
