package socialnetwork.service.wrappers;

import socialnetwork.domain.Message;

import java.time.LocalDate;

import static utils.Constants.DATE_FORMATTER;

public class Activity {
    ExternUser externUser;
    Message text;
    LocalDate localDate;

    public Activity(ExternUser externUser) {
        this.externUser = externUser;
        this.localDate = externUser.getLocalDate();
        this.text = null;
    }

    public Activity(Message text) {
        this.text = text;
        this.localDate = LocalDate.from(text.getData());
        this.externUser = null;
    }

    public ExternUser getExternUser() {
        return externUser;
    }

    public Message getText() {
        return text;
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    public boolean isFriend(){
        return externUser != null;
    }

    public boolean isMessage(){
        return text != null;
    }

    @Override
    public String toString() {
        return isMessage() ?
                localDate.format(DATE_FORMATTER) + " => Message sent: " + text.getMessage()
                :
                localDate.format(DATE_FORMATTER) + " => Friend added: " + externUser.getName();
    }
}
