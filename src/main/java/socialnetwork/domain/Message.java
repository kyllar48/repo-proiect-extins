package socialnetwork.domain;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

import static utils.Constants.DATE_TIME_FORMATTER;

public class Message extends Entity<Long>{
    private Long from;
    private List<Long> to;
    private String message;
    private LocalDateTime data;
    private Message reply;

    public Long getFrom() {
        return from;
    }

    public void setFrom(Long from) {
        this.from = from;
    }

    public List<Long> getTo() {
        return to;
    }

    public void setTo(List<Long> to) {
        this.to = to;
    }

    public String getMessage() {
        return message;
    }

    public LocalDateTime getData() {
        return data;
    }

    public Message getReply() {
        return reply;
    }

    public void setReply(Message reply) {
        this.reply=reply;
    }

    public Message(Long from, List<Long> to, String message, LocalDateTime data) {
        this.from = from;
        this.to = to;
        this.message = message;
        this.data = data;
        this.reply = null;
    }

    @Override
    public String toString() {
        return "Message received!" +
                "\nFrom: " + from +
                "\nTo: " + to +
                "\nSent at: " + data.format(DATE_TIME_FORMATTER) +
                "\nMessage: " + message;
    }

    public void removeMessageReceiver(Long id)
    {
        if(to.contains(id))
            to.remove(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message1 = (Message) o;
        return Objects.equals(getFrom(), message1.getFrom()) &&
                Objects.equals(getTo(), message1.getTo()) &&
                Objects.equals(getMessage(), message1.getMessage()) &&
                Objects.equals(getData(), message1.getData()) &&
                Objects.equals(getReply(), message1.getReply());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFrom(), getTo(), getMessage(), getData(), getReply());
    }
}
