package socialnetwork.service.wrappers;

import socialnetwork.domain.Message;

import static utils.Constants.DATE_TIME_FORMATTER;

public class MessageView {
    private Message message;
    private String date;
    private String text;

    public MessageView(Message message) {
        this.message = message;
        this.text=this.message.getMessage();
        this.date=this.message.getData().format(DATE_TIME_FORMATTER);
    }

    public Message getMessage() {
        return message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
