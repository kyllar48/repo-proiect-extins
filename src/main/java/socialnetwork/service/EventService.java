package socialnetwork.service;

import socialnetwork.domain.PublicEvent;
import socialnetwork.repository.Repository;
import utils.Constants;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class EventService {
    private Repository<Long, PublicEvent> repo;

    public EventService(Repository<Long, PublicEvent> repo) {
        this.repo = repo;
    }

    public PublicEvent addEvent(Long host, String name, String place, LocalDate date, LocalTime time) {
        PublicEvent publicEvent = new PublicEvent(host, name, place, LocalDateTime.of(date, time));
        List<PublicEvent> publicEventList = new ArrayList<>();
        this.repo.findAll().forEach(publicEventList::add);
        publicEvent.setId(publicEventList.stream().map(PublicEvent::getId)
                .reduce(0L, (x, y) -> x < y ? y : x) + 1);
        return this.repo.save(publicEvent);
    }

    public PublicEvent clearEvent(Long id_e) {
        return this.repo.delete(id_e);
    }

    public void attendToEvent(Long id_e, Long attendant) {
        PublicEvent publicEvent = this.repo.findOne(id_e);
        publicEvent.setAttendants(attendant, true);
        this.repo.update(publicEvent);
    }

    public void missEvent(Long id_e, Long attendant) {
        PublicEvent publicEvent = this.repo.findOne(id_e);
        publicEvent.setAttendants(attendant, false);
        this.repo.update(publicEvent);
    }

    public void updateEvents(Long deleted) {
        List<PublicEvent> eventsDeleted = new ArrayList<>();
        this.repo.findAll().forEach(e -> {
            if (e.getAttendants().contains(deleted))
                e.setAttendants(deleted, false);
        });
        this.repo.findAll().forEach(eventsDeleted::add);
        eventsDeleted = eventsDeleted.stream().filter(e -> e.getHost() == deleted).collect(Collectors.toList());
        eventsDeleted.forEach(e -> repo.delete(e.getId()));
    }

    public List<PublicEvent> getMyEvents(Long logged) {
        List<PublicEvent> events = new ArrayList<>();
        this.repo.findAll().forEach(events::add);
        return events.stream().filter(e -> e.getHost() == logged)
                .sorted(new Comparator<PublicEvent>() {
                    @Override
                    public int compare(PublicEvent o1, PublicEvent o2) {
                        Long rating1 = o1.getIminencyRating(logged);
                        Long rating2 = o2.getIminencyRating(logged);
                        if (rating1 < 0 && rating2 < 0)//passed events
                            return o2.getTime().compareTo(o1.getTime());
                        else if (rating1 < 0)
                            return 1;
                        else if (rating2 < 0)
                            return -1;
                        else return o1.getTime().compareTo(o2.getTime());
                    }
                })
                .collect(Collectors.toList());
    }

    public List<PublicEvent> getExternEvents(Long logged) {
        List<PublicEvent> allEvents = new ArrayList<>();
        this.repo.findAll().forEach(allEvents::add);
        List<PublicEvent> myEvents = getMyEvents(logged);
        return allEvents.stream().filter(e -> !myEvents.contains(e))
                .sorted(new Comparator<PublicEvent>() {
                    @Override
                    public int compare(PublicEvent o1, PublicEvent o2) {
                        Long rating1 = o1.getIminencyRating(logged);
                        Long rating2 = o2.getIminencyRating(logged);
                        if (rating1 < 0 && rating2 < 0)
                            return o2.getTime().compareTo(o1.getTime());
                        else if (rating1 < 0)
                            return 1;
                        else if (rating2 < 0)
                            return -1;
                        else return o1.getTime().compareTo(o2.getTime());
                    }
                })
                .collect(Collectors.toList());
    }

    public Long getImminentEventsNO(Long logged){
        List<PublicEvent> events = getMyEvents(logged);
        getExternEvents(logged).stream().filter(e -> e.getAttendants().contains(logged)).forEach(events::add);
        return events.stream().filter(e -> e.getIminencyRating(logged)<= Constants.IMMINENT_PERIOD && e.getIminencyRating(logged)>=0L).count();
    }
}
