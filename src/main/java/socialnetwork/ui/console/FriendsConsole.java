package socialnetwork.ui.console;

import socialnetwork.domain.Friendship;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.FriendshipService;
import socialnetwork.service.MessageService;
import socialnetwork.service.UserService;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.List;
import java.util.function.Predicate;

import static utils.Constants.DATE_FORMATTER;

public class FriendsConsole extends MainConsole {

    private User logged;

    public FriendsConsole(UserService userService, FriendshipService friendshipService, MessageService messageService, User logged) {
        super(userService, friendshipService, messageService);
        this.logged=logged;
    }

    @Override
    public void run() {
        while (true) {
            int option;
            System.out.println("1.Add Friend (give User)" + newLine
                    + "2.Remove Friend (give User)" + newLine
                    + "3.My friends" + newLine
                    + "4.Friends added in a specific period" + newLine
                    + "5.All my requests" + newLine
                    + "6.Answer a request" + newLine
                    + "7.Users blocked by me" + newLine
                    + "8.Unblock user" + newLine
                    + "9.Return to account main menu" + newLine
                    + "Choose an option from main menu by giving a number from the above."
                    + newLine);
            try {
                option = scanner.nextInt();
                switch (option) {
                    case 9: {
                        System.out.println("Closing friends menu..." + newLine);
                        return;
                    }
                    case 8: {
                        System.out.println("You need to unblock an user from the existing blocked users (view 8 in this menu to know which user to unblock)");
                        System.out.println("User will be given by his unique identifier!" + newLine);

                        Long id = scanner.nextLong();
                        Friendship friendship = this.friendshipService.unblockUser(logged, id);
                        if (friendship == null)
                            throw new ConsoleException("Given user does not exist or you did not block him yet!");
                        System.out.println("This user can send friend requests or messages to you now!" + newLine);
                        break;
                    }
                    case 7: {
                        Predicate<Friendship> isLogged = f -> f.getId().getRight() == logged.getId();
                        System.out.println("========================================================");
                        this.friendshipService.getBlockedUsers(logged,isLogged)
                                .stream().map(t -> new Tuple<>(t.getLeft().getLeft(),t.getRight()))
                                .forEach(elem ->{
                                    System.out.println(this.userService.findUser(elem.getLeft()) + "|Blocked since " + elem.getRight().format(DATE_FORMATTER) +"|");
                                });
                        System.out.println("===========================================================\n");
                        break;
                    }
                    case 6: {
                        System.out.println("Answering a request must have next format: id_sender;response");
                        System.out.println("Response must be one of the following: " +
                                "accept, reject or block." + newLine);
                        String line = scanner.next();
                        List<String> attributes = Arrays.asList(line.split(";"));
                        Long sender = Long.parseLong(attributes.get(0));
                        String response = attributes.get(1);
                        this.friendshipService.processRequest(logged, sender, response);
                        System.out.println();
                        break;
                    }
                    case 5: {
                        System.out.println("==================Requests===================");
                        this.friendshipService.getRequests(logged);
                        System.out.println("==============================================");
                        break;
                    }
                    case 4: {
                        System.out.println("Give a month and an year to search for friends.");
                        System.out.println("Month will be given as a number from 1 to 12!" + newLine);
                        Integer month = scanner.nextInt();
                        System.out.println(newLine + "Year will be given as a number greater than 0!" + newLine);
                        Integer year = scanner.nextInt();
                        if (1 <= month && month <= 12 && year > 0) {
                            System.out.println("===============What Search Found===================");
                            this.friendshipService.getMyFriendsFromGivenMonth(logged, month, year)
                                    .forEach(t ->
                                            System.out.println(t.getLeft().getFirstName()
                                                    + "|" + t.getLeft().getLastName() + "|" + t.getRight()));
                            System.out.println("====================================================");
                        } else throw new ConsoleException("Invalid month or year!" + newLine);
                        break;
                    }
                    case 3: {
                        System.out.println("===============My friends===================");
                        this.friendshipService.getMyFriends(logged)
                                .forEach(x -> {
                                    System.out.println(x.getLeft().getFirstName()
                                            + "|" + x.getLeft().getLastName() + "|" + x.getRight());
                                });
                        System.out.println("==============================================");
                        break;
                    }
                    case 2: {
                        System.out.println("Give a friend to remove." +
                                "Friend will be given by his unique identifier: id!" + newLine);
                        Long userID = scanner.nextLong();
                        User removed = this.userService.findUser(userID);
                        if (removed != null) {
                            this.friendshipService.removeFriend(logged, removed);
                            System.out.println("Friend removed successfully!" + newLine);
                        } else throw new ConsoleException("The user to be removed currently don't exist!"
                                + newLine);

                        break;
                    }
                    case 1: {
                        System.out.println("Give a friend to add." +
                                "Friend will be given by his unique identifier: id!" + newLine);
                        Long userID = scanner.nextLong();
                        User added = this.userService.findUser(userID);
                        if (added != null) {
                            Friendship friendship = this.friendshipService.addFriend(logged, added);
                            if (friendship == null)
                                System.out.println("Request sent!" + newLine);
                            else throw new ConsoleException("You already sent a request to that user!");
                        } else
                            throw new ConsoleException("The user to be added currently does not exist!"
                                    + newLine);
                        break;
                    }
                    default: {
                        System.out.println("No valid option!" + newLine);
                        break;
                    }
                }
            } catch (ValidationException validationException) {
                System.err.println(validationException + newLine);
            } catch (InputMismatchException inputMismatchException) {
                System.err.println("You need to give a number to proceed" + newLine);
                scanner.nextLine();
            } catch (IllegalArgumentException illegalArgumentException) {
                System.err.println(illegalArgumentException + newLine);
            } catch (ConsoleException consoleException) {
                System.err.println(consoleException + newLine);
            }
        }
    }
}
