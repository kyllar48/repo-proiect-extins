package socialnetwork.service;

import socialnetwork.domain.Tuple;
import socialnetwork.domain.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.Vector;

/**
 * CommunityService class.
 * Class used to incorporate all the aspects of
 * managing the relationships between the users of the community,
 * such as the number of the communities or the most sociable community
 * in the social network.
 */
public class CommunityService {

    private List<Long> userlist;
    private UserService userService;
    private int max_len;
    private int destination;

    public CommunityService(UserService userService) {
        this.userService = userService;
        userlist = new ArrayList<>();
        destination = -1;
        max_len = Integer.MIN_VALUE;
        for (User user : userService.getAll())
            this.userlist.add(user.getId());
    }

    /**
     * Method to obtain an user from the userlist in the program by his identifier.
     * @param id - an user's unique identifier.
     * @return the searched user.
     */
    private User ObtainUser(Long id) {
        return this.userService.findUser(id);
    }

    /**
     * Method to obtain a conex component from the users-friendships graph.
     * @param start - starting/current vertex for the component.
     * @param visited - vector used to verify if a vertex was already visited or not.
     * @param no - identification number for the conex component to be determined.
     */
    private void conexComponent(Long start, Vector<Tuple<Long, Integer>> visited, int no) {
        for (Tuple<Long, Integer> node : visited)
            if (node.getLeft() == start)
                if (node.getRight() == 0) {
                    node.setRight(no);
                    break;
                } else return;
        User user = ObtainUser(start);
        for (Long friend : user.getFriends()) {
            conexComponent(friend, visited, no);
        }
        return;
    }

    /**
     * Method to obtain the number of communities in the social network.
     * @return the searched number of communities.
     */
    public int CommunityNO() {
        Vector<Tuple<Long, Integer>> visited_nodes = new Vector<>();
        for (Long id : userlist)
            visited_nodes.add(new Tuple<>(id, 0));
        int conexNO = 0;
        for (Tuple<Long, Integer> node : visited_nodes)
            if (node.getRight() == 0) {
                conexNO++;
                conexComponent(node.getLeft(), visited_nodes, conexNO);
            }
        return conexNO;
    }

    /**
     * Method to translate all users friend's lists into an adjacency matrix.
     * @return an adjacency matrix.
     */
    private Vector<Tuple<Integer, Integer>>[] init_graph() {
        int nodesNO = this.userlist.size();
        Vector<Tuple<Integer, Integer>>[] graph = new Vector[nodesNO];
        for (int i = 0; i < nodesNO; i++) {
            graph[i] = new Vector<>();
        }
        for (int i = 0; i < nodesNO - 1; i++) {
            User logged = ObtainUser(this.userlist.get(i));
            for (int j = i + 1; j < nodesNO; j++) {
                if (logged.getFriends().contains(this.userlist.get(j))) {
                    graph[i].add(new Tuple<>(j, 1));
                    graph[j].add(new Tuple<>(i, 1));
                }
            }
        }
        return graph;
    }

    /**
     * Method to determine the size and the final extremity
     * of the longest road in the users-friendships graph.
     * @param graph - users-friendships graph as an adjacency matrix.
     * @param start - starting/current vertex from the road.
     * @param prev_len - previous-iterations length of the road stored in program.
     * @param visited - vector used to verify if a vertex was already visited or not.
     */
    private void LongestRoadFromGivenVertex(Vector<Tuple<Integer, Integer>>[] graph, int start, int prev_len, Vector<Boolean> visited) {
        visited.setElementAt(true, start);
        int current_len = 0;
        int current_dest = -1;
        Tuple<Integer, Integer> adjacent;
        for (int i = 0; i < graph[start].size(); i++) {
            adjacent = graph[start].elementAt(i);
            if (!visited.elementAt(adjacent.getLeft())) {
                current_len = prev_len + adjacent.getRight();
                current_dest = adjacent.getLeft();
                LongestRoadFromGivenVertex(graph, adjacent.getLeft(), current_len, visited);

            }
            if (max_len <= current_len) {
                max_len = current_len;
                destination = current_dest;
            }
            current_len = 0;
        }
        visited.setElementAt(false, start);
    }

    /**
     * Method to store a road based on the extremities and the exact length of the road.
     * @param graph - users-friendships graph as an adjacency matrix.
     * @param start - starting/current vertex from the road.
     * @param prev_len - previous-iterations length of the road stored in program.
     * @param visited - vector used to verify if a vertex was already visited or not.
     * @param stack - vertexes that compose the actual searched path.
     * @return true - the path was found.
     *         false - the path was not found, the vertex will be removed from the stack.
     */
    private Boolean PathVertexes(Vector<Tuple<Integer, Integer>>[] graph, int start, int prev_len, Vector<Boolean> visited, Stack<Integer> stack) {
        visited.setElementAt(true, start);
        int current_len = 0;
        int current_dest = -1;
        Tuple<Integer, Integer> adjacent;
        stack.push(start);
        for (int i = 0; i < graph[start].size(); i++) {
            adjacent = graph[start].elementAt(i);
            if (!visited.elementAt(adjacent.getLeft())) {
                current_len = prev_len + adjacent.getRight();
                current_dest = adjacent.getLeft();
                Boolean searched_path = PathVertexes(graph, current_dest, current_len, visited, stack);
                if (!searched_path)
                    stack.pop();
                else return true;
            }
            if (max_len == current_len && current_dest == destination)
                return true;
            current_len = 0;
        }
        visited.setElementAt(false, start);
        return false;
    }

    /**
     * Method to determine all the indexes that compose the longest road in the users-friendships graph
     * @return a stack containing indexes used to find out the unique identifiers for
     * the searched users.
     */
   private Stack<Integer> getPath() {
        Vector<Tuple<Integer, Integer>>[] graph = init_graph();
        int nodesNO = this.userlist.size();


        int final_destination = -1;
        int final_source = -1;
        for (int i = 0; i < nodesNO; i++) {
            Vector<Boolean> visited = new Vector<>();
            for (int j = 0; j < nodesNO; j++)
                visited.add(false);
            int copy_max_len = max_len;
            destination = -1;
            LongestRoadFromGivenVertex(graph, i, 0, visited);
            if (copy_max_len != max_len && destination != -1) {
                final_destination = destination;
                final_source=i;
            }
        }
        destination = final_destination;
        Stack<Integer> stack = new Stack<>();
        Vector<Boolean> visited = new Vector<>();
        for (int j = 0; j < nodesNO; j++)
            visited.add(false);
        PathVertexes(graph,final_source,0,visited,stack);
        stack.push(final_destination);
        return stack;
    }

    /**
     * Method to find out the most sociable community in the network.
     * @return list of users incorporated in the most sociable community.
     */
    public List<User> MostSociableCommunity()
    {
        List<User> users=new ArrayList<>();
        Stack<Integer> userPositions=getPath();
        userPositions.forEach((Integer pos) ->{
            users.add(ObtainUser(this.userlist.get(pos)));
        });
        return users;
    }
}

