package socialnetwork.domain.validators;

import socialnetwork.domain.Message;

import java.time.LocalDateTime;

public class MessageValidator implements Validator<Message> {
    @Override
    public void validate(Message entity) throws ValidationException {
        if (entity.getFrom() == null || entity.getTo() == null || entity.getData() == null || entity.getMessage() == null)
            throw new ValidationException("One or more message informations were missing!");
        if (entity.getId() <= 0)
            throw new ValidationException("ID must not be null or negative!");
        if (entity.getTo().size() == 0)
            throw new ValidationException("You need to give users(that did not block you) to send the message to them!");
        if (entity.getTo().contains(entity.getFrom()))
            throw new ValidationException("You cannot send messages to yourself!");
        if (entity.getData().isAfter(LocalDateTime.now()))
            throw new ValidationException("The message is from future or what?!");
        if (entity.getMessage().length() == 0)
            throw new ValidationException("You cannot send an empty message!");
        if(entity.getMessage().matches("[;\\|]"))
            throw new ValidationException("Message cannot contain ';' or '|' !");
    }
}
