package socialnetwork.service.wrappers;

import socialnetwork.domain.User;

import java.time.LocalDate;

public class FriendRequest {

    private User user;
    private String name;
    private LocalDate localDate;
    private String status;

    public FriendRequest(User user, LocalDate localDate, String status) {
        this.user = user;
        this.localDate = localDate;
        this.status = status;
        this.name = user.getName();
    }

    public User getUser() {
        return user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    public void setLocalDate(LocalDate localDate) {
        this.localDate = localDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
