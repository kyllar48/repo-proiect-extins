package socialnetwork.service.wrappers;

import socialnetwork.domain.User;

import java.time.LocalDate;

public class ExternUser {
    private User user;
    private String name;
    private LocalDate localDate;

    public ExternUser(User user, LocalDate localDate) {
        this.user=user;
        this.name = this.user.getName();
        this.localDate = localDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    public void setLocalDate(LocalDate localDate) {
        this.localDate = localDate;
    }

    public User getUser(){
        return user;
    }
}
