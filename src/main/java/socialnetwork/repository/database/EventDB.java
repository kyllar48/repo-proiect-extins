package socialnetwork.repository.database;

import socialnetwork.domain.PublicEvent;
import socialnetwork.domain.validators.Validator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class EventDB extends AbstractDBRepository<Long, PublicEvent> {
    public EventDB(Validator<PublicEvent> validator, String url) {
        super(validator, url);
    }

    private List<Long> getAttendants(Long id_event) {
        List<Long> attendantsList = new ArrayList<>();
        try {
            String mySQL = "SELECT id_u FROM [First try].Attendant WHERE id_e=" + id_event;
            PreparedStatement statement = connection.prepareStatement(mySQL);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                attendantsList.add(resultSet.getLong(1));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return attendantsList;
    }

    @Override
    protected void load_data() {
        try {
            String mySQL = "SELECT * FROM [First try].Event";
            PreparedStatement statement = connection.prepareStatement(mySQL);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                PublicEvent publicEvent = new PublicEvent(resultSet.getLong(2), resultSet.getString(3), resultSet.getString(4),
                        resultSet.getTimestamp(5).toLocalDateTime());
                Long id_e = resultSet.getLong(1);
                publicEvent.setId(id_e);
                getAttendants(id_e).forEach(id -> publicEvent.setAttendants(id, true));
                super.save(publicEvent);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public PublicEvent save(PublicEvent entity){
        PublicEvent e=super.save(entity);
        if(e==null){
            addToDB(entity);
        }
        return e;
    }

    @Override
    protected void addToDB(PublicEvent entity) {
        try {

            Statement statement = connection.createStatement();
            statement.executeUpdate("SET IDENTITY_INSERT [First try].Event ON; " +
                    "INSERT INTO [First try].Event(id_e,host,name,place,e_time) " +
                    "VALUES (" +
                    entity.getId() +
                    "," +
                    entity.getHost() +
                    "," +
                    "'" + entity.getName() + "'" +
                    "," +
                    "'" + entity.getPlace() + "'" +
                    "," +
                    "'" + entity.getTimeToString() + "'" +
                    ")");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    protected void clearFromDB(PublicEvent entity) {
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate("DELETE FROM [First try].Attendant " +
                    "WHERE id_e=" + entity.getId());
            statement.executeUpdate("DELETE FROM [First try].Event " +
                    "WHERE id_e=" + entity.getId());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public PublicEvent update(PublicEvent entity) {
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate("DELETE FROM [First try].Attendant " +
                    "WHERE id_e=" + entity.getId());
            if (entity.getAttendants().size() != 0) {
                String attendants = entity.getAttendants().stream()
                        .map(id -> "(".concat(String.valueOf(entity.getId())).concat(",").concat(String.valueOf(id)).concat(")"))
                        .reduce("", (x, y) -> x.concat(",").concat(y))
                        .substring(1);
                statement.executeUpdate("INSERT INTO [First Try].Attendant(id_e, id_u) VALUES " + attendants);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return entity;
    }
}
