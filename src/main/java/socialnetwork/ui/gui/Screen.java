package socialnetwork.ui.gui;

import java.util.List;

public interface Screen {

    public void setScreenParent(ScreensController screenPage);

    public void setEnvironment(List<Object> args);

}
