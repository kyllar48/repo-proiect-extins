package utils;

import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

public class Constants {
    public static final DateTimeFormatter DATE_FORMATTER =
            new DateTimeFormatterBuilder().parseCaseInsensitive()
            .append(DateTimeFormatter.ofPattern("yyyy-MM-dd")).toFormatter();
    public static final DateTimeFormatter DATE_TIME_FORMATTER =
            new DateTimeFormatterBuilder().parseCaseInsensitive()
            .append(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")).toFormatter();
    public static final DateTimeFormatter TIME_FORMATTER =
            new DateTimeFormatterBuilder().parseCaseInsensitive()
            .append(DateTimeFormatter.ofPattern("HH:mm")).toFormatter();
    public static final String URL =
            "jdbc:sqlserver://localhost:51121;databaseName=Network;integratedSecurity=true;";
    public static final String screen1ID="main";
    public static final String screen1Res="/view/mainWindow.fxml";
    public static final String screen2ID="myAccount";
    public static final String screen2Res="/view/accountWindow.fxml";
    public static final String screen3ID="friendAccount";
    public static final String screen3Res="/view/userInfoPage.fxml";
    public static final String styling1Res="/styling/style.css";
    public static final Long IMMINENT_PERIOD=7L;
    public static final String PICS_PATH = "/Java/Laborator/src/pics/";
    public static final String REPORTS_PATH = "/Java/Laborator/src/reports/";
}
