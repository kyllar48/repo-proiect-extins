package socialnetwork.repository.database;

import socialnetwork.domain.Message;
import socialnetwork.domain.validators.Validator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static utils.Constants.DATE_TIME_FORMATTER;

public class MessageDB extends AbstractDBRepository<Long, Message> {

    public MessageDB(Validator<Message> validator, String url) {
        super(validator, url);
        setReplies();
    }


    private List<Long> getReceivers(Long id_message) {
        List<Long> to = new ArrayList<>();
        try {
            String mySQL = "SELECT id_receiver FROM [First try].Mail WHERE id_m=" + id_message;
            PreparedStatement statement = connection.prepareStatement(mySQL);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                to.add(resultSet.getLong(1));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return to;
    }

    @Override
    protected void load_data() {
        {
            try {
                String mySQL = "SELECT * FROM [First try].Message";
                PreparedStatement statement = connection.prepareStatement(mySQL);
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    Message message = new Message(resultSet.getLong(2), null, resultSet.getString(3), resultSet.getTimestamp(4).toLocalDateTime());
                    Long msgID = resultSet.getLong(1);
                    message.setId(msgID);
                    message.setTo(getReceivers(msgID));
                    super.save(message);
                }

            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    private void setReplies() {
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT id_m, id_reply FROM [First try].Message");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Long id_m = resultSet.getLong(1);
                Long id_reply = resultSet.getLong(2);
                if (id_reply == 0)
                    continue;
                super.findOne(id_m).setReply(super.findOne(id_reply));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public Message save(Message entity) {
        Message e = super.save(entity);
        if (e == null) {
            addToDB(entity);
        }
        return e;
    }

    @Override
    protected void addToDB(Message entity) {
        try {

            Statement statement = connection.createStatement();
            statement.executeUpdate("SET IDENTITY_INSERT [First try].Message ON; " +
                    "INSERT INTO [First try].Message(id_m,sender,text,data) " +
                    "VALUES (" + entity.getId() + "," + entity.getFrom() + ",'" + entity.getMessage() + "','" + entity.getData().format(DATE_TIME_FORMATTER) + "')");
            if (entity.getId() > 1000)//is a reply
                statement.executeUpdate("UPDATE [First try].Message " + "SET id_reply=" + entity.getId() + " WHERE id_m=" + entity.getId() / 1000);
            String valuesMail = entity.getTo()
                    .stream().map(receiver -> "(" + entity.getId() + "," + receiver + ")")
                    .reduce("", (x, y) -> x.concat(",").concat(y));
            statement.executeUpdate("INSERT INTO [First try].Mail(id_m,id_receiver) VALUES " + valuesMail.substring(1));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    protected void clearFromDB(Message entity) {
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate("DELETE FROM [First try].Mail " +
                    "WHERE id_m=" + entity.getId());
            statement.executeUpdate("DELETE FROM [First try].Message " +
                    "WHERE id_m=" + entity.getId());
            if (entity.getId() >= 1000)//is a reply
                statement.executeUpdate("UPDATE [First try].Message SET id_reply=NULL WHERE id_m=" + (entity.getId() / 1000));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
