package socialnetwork.repository.database;

import socialnetwork.domain.Friendship;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.validators.Validator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class FriendshipDB extends AbstractDBRepository<Tuple<Long, Long>, Friendship> {


    public FriendshipDB(Validator<Friendship> validator, String url) {
        super(validator, url);
    }

    @Override
    protected void load_data() {
        {
            try {
                String mySQL = "SELECT * FROM [First try].Friendship";
                PreparedStatement statement = connection.prepareStatement(mySQL);
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    Friendship friendship=new Friendship();
                    friendship.setId(new Tuple<>(resultSet.getLong(2), resultSet.getLong(3)));
                    friendship.setDate(resultSet.getDate(1).toLocalDate());
                    friendship.setStatus(resultSet.getString(4));
                    super.save(friendship);
                }

            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    @Override
    public Friendship save(Friendship entity){
        Friendship e=super.save(entity);
        if(e==null){
            addToDB(entity);
        }
        return e;
    }

    @Override
    protected void addToDB(Friendship entity) {
        try {
            String mySQL;
            mySQL = "INSERT INTO [First try].Friendship(id_logged,id_added,friendship_start,status)"
                    + "VALUES" +
                    "(" + entity.getId().getLeft() + "," + entity.getId().getRight() + ",'" + entity.getDate() + "','" + entity.getStatus() + "')";
            Statement statement = connection.createStatement();
            statement.executeUpdate(mySQL);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    protected void clearFromDB(Friendship entity) {
        try {
            String mySQL;
            mySQL = "DELETE FROM [First try].Friendship " +
                    "WHERE (id_logged=" + entity.getId().getLeft() + " AND id_added=" + entity.getId().getRight()
                    +") OR (id_logged=" + entity.getId().getRight() + " AND id_added=" + entity.getId().getLeft() +")";
            Statement statement = connection.createStatement();
            statement.executeUpdate(mySQL);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public Friendship update(Friendship entity){
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate("UPDATE [First try].Friendship " +
                    "SET status='" + entity.getStatus() + "', friendship_start='" +entity.getDate()+"' "+
                    "WHERE (id_logged=" + entity.getId().getLeft() + " AND id_added=" + entity.getId().getRight()
                            +") OR (id_logged=" + entity.getId().getRight() + " AND id_added=" + entity.getId().getLeft() +")");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return entity;
    }

}
