package socialnetwork.repository.file;

import socialnetwork.domain.Friendship;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.Validator;

import java.time.*;
import java.util.Arrays;
import java.util.List;

public class FriendshipFile extends AbstractFileRepository<Tuple<java.lang.Long, java.lang.Long>, Friendship> {


    public FriendshipFile(String fileName, Validator<Friendship> validator) {
        super(fileName,validator);
    }

    /**
     * extract Entity - method for extracting a friendship from a list of attributes.
     * @param attributes
     *          must not be null
     *          A friendship will be extracted from this list
     * @return  A 'brute' friendship, ready to be validated.
     */
    @Override
    public Friendship extractEntity(List<String> attributes) {
        Friendship friendship = new Friendship();
        friendship.setId(new Tuple<>(java.lang.Long.parseLong(attributes.get(0)), java.lang.Long.parseLong(attributes.get(1))));
        List<String> dateCoords= Arrays.asList(attributes.get(2).split("-"));
        LocalDate localDate;
        localDate = LocalDate.of(Integer.valueOf(dateCoords.get(0)),
                Integer.valueOf(dateCoords.get(1)),
                Integer.valueOf(dateCoords.get(2)));
        friendship.setDate(localDate);
        return friendship;
    }

    /**
     * create entity as string - method for encoding a friendship to string
     * @param entity
     *          must not be null
     *          must be a friendship
     * @return a string which represent a friendship.
     */
    @Override
    protected String createEntityAsString(Friendship entity) {
        return entity.getId().getLeft() + ";" + entity.getId().getRight() + ";" + entity.getDate();
    }

}
