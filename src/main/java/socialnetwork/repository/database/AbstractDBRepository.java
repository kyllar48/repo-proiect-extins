package socialnetwork.repository.database;

import socialnetwork.domain.Entity;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.memory.InMemoryRepository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public abstract class AbstractDBRepository<ID, E extends Entity<ID>> extends InMemoryRepository<ID, E> {

    private String url;
    protected Connection connection;

    public AbstractDBRepository(Validator<E> validator, String url) {
        super(validator);
        this.url = url;
        try {
            this.connection= DriverManager.getConnection(url);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        load_data();
    }

    protected abstract void load_data();

    protected abstract void addToDB(E entity);

    @Override
    public E delete(ID id){
        E e=super.delete(id);
        if(e != null){
            clearFromDB(e);
        }
        return e;
    }

    protected abstract void clearFromDB(E entity);

}
